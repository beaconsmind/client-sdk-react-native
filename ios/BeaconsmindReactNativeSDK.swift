import Beaconsmind
import CoreLocation

@objc(BeaconsmindReactNativeSDK)
class BeaconsmindReactNativeSDK: NSObject, BeaconsmindDelegate {

    func beaconsmind(_ beaconsmind: Beaconsmind, onContextChanged context: APIContext?) {
    }

    @objc(requestPermissions:withRejecter:)
    func requestPermissions(resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock) -> Void {
        Beaconsmind.default.registerForPushNotifications()
        resolve(true)
    }

    // Deprecated, use initialize instead
    @objc(start:withResolver:withRejecter:)
    func start(options: NSDictionary, resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock) -> Void {
        guard let hostname = options["hostname"] as? String, let appVersion: String = options["appVersion"] as? String else {
            reject("500", "Invalid arguments", nil)
            return;
        }

        do {
            try
            Beaconsmind.default.start(
                delegate: self,
                appVersion: appVersion,
                hostname: hostname
            )
            resolve(true)
        } catch {
            reject("500", "start failed", error)
        }
    }
  
    @objc(initialize:withResolver:withRejecter:)
    func initialize(options: NSDictionary, resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock) -> Void {
        guard let hostname = options["hostname"] as? String, let appVersion: String = options["appVersion"] as? String else {
            reject("500", "Invalid arguments", nil)
            return;
        }

        do {
            try
            Beaconsmind.default.start(
                delegate: self,
                appVersion: appVersion,
                hostname: hostname
            )
            resolve(true)
        } catch {
            reject("500", "initialize failed", error)
        }
    }
  
    @objc(initializeDevelop:withResolver:withRejecter:)
    func initializeDevelop(options: NSDictionary, resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock) -> Void {
        guard let hostname = options["hostname"] as? String, let appVersion: String = options["appVersion"] as? String else {
            reject("500", "Invalid arguments", nil)
            return;
        }

        do {
            try
            Beaconsmind.default.startDevelop(
                delegate: self,
                appVersion: appVersion,
                hostname: hostname
            )
            resolve(true)
        } catch {
            reject("500", "initializeDevelop failed", error)
        }
    }

    @objc static func requiresMainQueueSetup() -> Bool {
        return false
    }

    @objc(signup:withResolver:withRejecter:)
    func signup(options: NSDictionary, resolve: @escaping RCTPromiseResolveBlock,reject: @escaping RCTPromiseRejectBlock) -> Void {
        guard let args = options as? [String: Any]
        else {
            reject("500", "Invalid arguments", nil)
            return
        }
        do {
            guard let username = args["username"] as? String else {
                reject("500", "username is required", nil)
                return
            }
            guard let firstName = args["firstName"] as? String else {
                reject("500", "firstName is required", nil)
                return
            }
            guard let lastName = args["lastName"] as? String else {
                reject("500", "lastName is required", nil)
                return
            }
            guard let password = args["password"] as? String else {
                reject("500", "password is required", nil)
                return
            }
            guard let confirmPassword = args["confirmPassword"] as? String else {
                reject("500", "confirmPassword is required", nil)
                return
            }
            
            let language = args["language"] as? String
            let gender = args["gender"] as? String
            let favoriteStoreID = args["favoriteStoreID"] as? Int
            let birthDateSeconds = args["birthDateSeconds"] as? Double
            let countryCode = args["countryCode"] as? String

            try Beaconsmind.default.signup(
                username: username,
                firstName: firstName,
                lastName: lastName,
                password: password,
                confirmPassword: confirmPassword,
                language: language,
                gender: gender,
                favoriteStoreID: favoriteStoreID,
                birthDate:  getDateDayFromSeconds(seconds: birthDateSeconds),
                countryCode: countryCode
            ) { resp in
                    switch resp {
                    case let .success(ctx):
                        resolve([
                            "userId": ctx.userID,
                        ])
                    case let .failure(error):
                        reject("500", "signup failed: ", error)
                    }
                }
        }  catch  {
            reject("500", "signup failed!", error)
        }
    }

    @objc(importAccount:withResolver:withRejecter:)
    func importAccount(options: NSDictionary, resolve: @escaping RCTPromiseResolveBlock,reject: @escaping RCTPromiseRejectBlock) -> Void {
      guard let args = options as? [String: Any]
      else {
          reject("500", "invalid arguments", nil)
          return
      }
      do {
        guard let id = args["id"] as? String else {
          reject("500", "id is required", nil)
          return
        }
        guard let email = args["email"] as? String else {
          reject("500", "email is required", nil)
          return
        }
        
        let firstName = args["firstName"] as? String
        let lastName = args["lastName"] as? String
        let language = args["language"] as? String
        let gender = args["gender"] as? String
        let birthDateSeconds = args["birthDateSeconds"] as? Double
        
          try
          Beaconsmind.default.importAccount(
              id: id,
              email: email,
              firstName: firstName,
              lastName: lastName,
              birthDate:  getDateDayFromSeconds(seconds: birthDateSeconds)?.date,
              language: language,
              gender: gender) { resp in
                  switch resp {
                  case let .success(ctx):
                      resolve([
                          "userId": ctx.userID,
                      ])
                  case let .failure(error):
                      reject("500", "importAccount failed", error)
                  }
              }
      }  catch  {
          reject("500", "importAccount failed", error)
      }
    }

    @objc(login:withResolver:withRejecter:)
    func login(options: NSDictionary, resolve: @escaping RCTPromiseResolveBlock,reject: @escaping RCTPromiseRejectBlock) -> Void {
      guard let args = options as? [String: Any]
      else {
          reject("500", "invalid arguments", nil)
          return
      }
      do {
        guard let username = args["username"] as? String else {
          reject("500", "username is required", nil)
          return
        }
        guard let password = args["password"] as? String else {
          reject("500", "password is required", nil)
          return
        }
        
        try Beaconsmind.default.login(
            username: username,
            password: password) { resp in
                switch resp {
                case let .success(ctx):
                    resolve([
                        "userId": ctx.userID,
                    ])
                case let .failure(error):
                    reject("500", "login failed", error)
                }
            }
      } catch  {
          do {
              /// TODO(korzonkiee): call resolve function in the logout callback.
              try Beaconsmind.default.logout(callback: nil)
              resolve(true)
          } catch {
              reject("500", "logout failed", error)
          }
      }
    }

    @objc(logout:withRejecter:)
    func logout(resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock) -> Void {
      do {
          /// TODO(korzonkiee): call resolve function in the logout callback.
          try Beaconsmind.default.logout(callback: nil)
          resolve(true)
      } catch {
          reject("500", "logout failed", error)
      }
    }

    @objc(getOAuthContext:withRejecter:)
    func getOAuthContext(resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock) -> Void {
        let ctx = Beaconsmind.default.getAPIContext()
        if ctx != nil {
            resolve([
                "userId": ctx!.userID,
            ])
        } else {
            resolve(nil)
        }
    }

    @objc(updateHostname:withResolver:withRejecter:)
    func updateHostname(options: NSDictionary, resolve: @escaping RCTPromiseResolveBlock,reject: @escaping RCTPromiseRejectBlock) -> Void {
      guard let args = options as? [String: Any]
      else {
          reject("500", "invalid arguments", nil)
          return
      }
      
      guard let hostname = args["hostname"] as? String else {
        reject("500", "hostname is required", nil)
        return
      }
      
      Beaconsmind.default.updateHostname(hostname: hostname)
      resolve(true)
    }

    @objc(registerDeviceToken:withResolver:withRejecter:)
    func registerDeviceToken(options: NSDictionary, resolve: @escaping RCTPromiseResolveBlock,reject: @escaping RCTPromiseRejectBlock) -> Void {
      guard let args = options as? [String: Any]
      else {
          reject("500", "invalid arguments", nil)
          return
      }
      do {
        guard let deviceToken = args["deviceToken"] as? String else {
          reject("500", "deviceToken is required", nil)
          return
        }
        
          try Beaconsmind.default.register(deviceToken: deviceToken) { resp in
              switch resp {
              case .success:
                  resolve(true)
              case let .failure(error):
                  reject("500", "registerDeviceToken failed", error)
              }
          }
      } catch {
          reject("500", "registerDeviceToken failed", error)
      }
    }

    @objc(getProfile:withRejecter:)
    func getProfile(resolve: @escaping RCTPromiseResolveBlock,reject: @escaping RCTPromiseRejectBlock) -> Void {
        do {
            try
            _ = Beaconsmind.default.getProfile() { resp in
                switch resp {
                case let .success(profile):
                    let encoder = JSONEncoder()
                    encoder.dateEncodingStrategy = .iso8601
                    let data = try! encoder.encode(profile)
                    let dict = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String, Any>
                    // TODO: date fields are returned like this: -393724800
                    resolve(dict)
                case let .failure(error):
                    reject("500", "getProfile failed!", error)
                }
            }
        }  catch {
            reject("500", "getProfile failed", error)
        }
    }

    @objc(updateProfile:withResolver:withRejecter:)
    func updateProfile(options: NSDictionary, resolve: @escaping RCTPromiseResolveBlock,reject: @escaping RCTPromiseRejectBlock) -> Void {
        guard let args = options as? [String: Any]
        else {
            reject("500", "Invalid arguments!", nil)
            return
        }
        do {
            let firstName = args["firstName"] as! String
            let lastName = args["lastName"] as! String
            let birthDateSeconds = args["birthDateSeconds"] as? Double
            let city = args["city"] as? String
            let country = args["country"] as? String
            let disablePushNotifications = args["disablePushNotifications"] as? Bool
            let favoriteStoreID = args["favoriteStoreID"] as? Int
            let gender = args["gender"] as? String
            let houseNumber = args["houseNumber"] as? String
            let landlinePhone = args["landlinePhone"] as? String
            let language = args["language"] as? String
            let phoneNumber = args["phoneNumber"] as? String
            let street = args["street"] as? String
            let zipCode = args["zipCode"] as? String

            try
            _ = Beaconsmind.default.updateProfile(
                firstName: firstName,
                lastName: lastName,
                birthDate:  getDateDayFromSeconds(seconds: birthDateSeconds)?.date,
                city: city,
                country: country,
                disablePushNotifications: disablePushNotifications,
                favoriteStoreID: favoriteStoreID,
                gender: gender,
                houseNumber: houseNumber,
                landlinePhone: landlinePhone,
                language: language,
                phoneNumber: phoneNumber,
                street: street,
                zipCode: zipCode) { resp in
                    switch resp {
                    case let .success(profile):
                        let encoder = JSONEncoder()
                        let data = try! encoder.encode(profile)
                        let dict = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String, Any>
                        // TODO: date fields are returned like this: -393724800
                        resolve(dict)
                        break
                    case let .failure(error):
                        reject("500", "updateProfile failed", error)
                    }
                }
        } catch {
            reject("500", "updateProfile failed", error)
        }
    }

    @objc(changePassword:withResolver:withRejecter:)
    func changePassword(options: NSDictionary, resolve: @escaping RCTPromiseResolveBlock,reject: @escaping RCTPromiseRejectBlock) -> Void {
        guard let args = options as? [String: Any]
        else {
            reject("500", "Invalid arguments!", nil)
            return
        }
        do {
            let oldPassword = args["oldPassword"] as! String
            let newPassword = args["newPassword"] as! String

            try
            _ = Beaconsmind.default.changePassword(
                newPassword: newPassword,
                oldPassword: oldPassword
                ) { resp in
                    switch resp {
                    case let .success(profile):
                        let encoder = JSONEncoder()
                        let data = try! encoder.encode(profile)
                        let dict = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String, Any>
                        resolve(dict)
                        break
                    case let .failure(error):
                        reject("500", "changePassword failed", error)
                    }
                }
        } catch {
            reject("500", "changePassword failed", error)
        }
    }

    @objc(loadOffers:withRejecter:)
    func loadOffers(resolve: @escaping RCTPromiseResolveBlock,reject: @escaping RCTPromiseRejectBlock) -> Void {
        do {
            let request = API.Offers.OffersGetOffers.Request()
            _ = try Beaconsmind.default.apiRequest(request) { res in
                switch res {
                case let .success(offers):
                    let data = try! JSONEncoder().encode(offers.success)
                    let dict = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [Dictionary<String, Any>]
                    resolve(dict)
                case let .failure(error):
                    reject("500", "loadOffers failed", error)
                }
            }
        } catch {
            reject("500", "loadOffers failed", error)
        }
    }

    @objc(loadOffer:withResolver:withRejecter:)
    func loadOffer(options: NSDictionary, resolve: @escaping RCTPromiseResolveBlock,reject: @escaping RCTPromiseRejectBlock) -> Void {
        guard let args = options as? [String: Any],
              let offerId = args["offerId"] as? Int
        else {
            reject("500", "Invalid arguments", nil)
            return
        }
        do {
            let request = API.Offers.OffersGetOffer.Request(offerId: offerId)
            _ = try Beaconsmind.default.apiRequest(request) { res in
                switch res {
                case let .success(response):
                    let data = try! JSONEncoder().encode(response.success)
                    let dict = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String, Any>
                    resolve(dict)
                case let .failure(error):
                    reject("500", "loadOffers failed", error)
                }
            }
        } catch {
            reject("500", "loadOffer failed", error)
        }
    }

    @objc(markOfferAsRead:withResolver:withRejecter:)
    func markOfferAsRead(options: NSDictionary, resolve: @escaping RCTPromiseResolveBlock,reject: @escaping RCTPromiseRejectBlock) -> Void {
        guard let args = options as? [String: Any],
              let offerId = args["offerId"] as? Int
        else {
            reject("500", "Invalid arguments", nil)
            return
        }
        do {
            try Beaconsmind.default.markOfferAsRead(offerID: offerId) { resp in
                switch resp {
                case .success:
                    resolve(true)
                case let .failure(error):
                    reject("500", "markOfferAsRead failed", error)
                }
            }
        } catch {
            reject("500", "markOfferAsRead failed", error)
        }
    }

    @objc(markOfferAsReceived:withResolver:withRejecter:)
    func markOfferAsReceived(options: NSDictionary, resolve: @escaping RCTPromiseResolveBlock,reject: @escaping RCTPromiseRejectBlock) -> Void {
        guard let args = options as? [String: Any],
              let offerId = args["offerId"] as? Int
        else {
            reject("500", "Invalid arguments", nil)
            return
        }
        do {
            try Beaconsmind.default.markOfferAsReceived(offerID: offerId) { resp in
                switch resp {
                case .success:
                    resolve(true)
                case let .failure(error):
                    reject("500", "markOfferAsReceived failed", error)
                }
            }
        } catch {
            reject("500", "markOfferAsReceived failed", error)
        }
    }

    @objc(markOfferAsRedeemed:withResolver:withRejecter:)
    func markOfferAsRedeemed(options: NSDictionary, resolve: @escaping RCTPromiseResolveBlock,reject: @escaping RCTPromiseRejectBlock) -> Void {
        guard let args = options as? [String: Any],
              let offerId = args["offerId"] as? Int
        else {
            reject("500", "Invalid arguments", nil)
            return
        }
        do {
            try Beaconsmind.default.markOfferAsRedeemed(offerID: offerId) { resp in
                switch resp {
                case .success:
                    resolve(true)
                case let .failure(error):
                    reject("500", "markOfferAsRedeemed failed", error)
                }
            }
        } catch {
            reject("500", "markOfferAsRedeemed failed", error)
        }
    }
    
    @objc(markOfferAsClicked:withResolver:withRejecter:)
    func markOfferAsClicked(options: NSDictionary, resolve: @escaping RCTPromiseResolveBlock,reject: @escaping RCTPromiseRejectBlock) -> Void {
        guard let args = options as? [String: Any],
              let offerId = args["offerId"] as? Int
        else {
            reject("500", "Invalid arguments", nil)
            return
        }
        do {
            try Beaconsmind.default.markOfferAsClicked(offerID: offerId) { resp in
                switch resp {
                case .success:
                    resolve(true)
                case let .failure(error):
                    reject("500", "markOfferAsClicked failed", error)
                }
            }
        } catch {
            reject("500", "markOfferAsClicked failed", error)
        }
    }

    @objc(startListeningBeacons:withRejecter:)
    func startListeningBeacons(resolve: @escaping RCTPromiseResolveBlock, reject: @escaping RCTPromiseRejectBlock) -> Void {
        onMainThread { [unowned self] in
            do {
                try Beaconsmind.default.startListeningBeacons(delegate: self)
                resolve(true)
            } catch {
                reject("500", "startListeningBeacons failed", error)
            }
        }
    }

    @objc(stopListeningBeacons:withRejecter:)
    func stopListeningBeacons(resolve: @escaping RCTPromiseResolveBlock, reject: @escaping RCTPromiseRejectBlock) -> Void {
        Beaconsmind.default.stopListeningBeacons()
        resolve(true)
    }

    @objc(getBeaconsSummary:withRejecter:)
    func getBeaconsSummary(resolve: @escaping RCTPromiseResolveBlock, reject: @escaping RCTPromiseRejectBlock) -> Void {
        do {
            let request = API.Configuration.ConfigurationGetBeacons.Request()
            _ = try Beaconsmind.default.apiRequest(request) { res in
                switch res {
                case let .success(response):
                    let data = try! JSONEncoder().encode(response.success)
                    let result = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [Dictionary<String, Any>]
                    resolve(result)
                case let .failure(error):
                    reject("500", "getBeaconsSummary failed!", error)
                }
            }
        } catch {
            reject("500", "getBeaconsSummary failed", error)
        }
    }
    
    @objc(getBeaconContactsSummary:withRejecter:)
    func getBeaconContactsSummary(resolve: @escaping RCTPromiseResolveBlock, reject: @escaping RCTPromiseRejectBlock) -> Void {
        do {
            let beaconContactsSummary = Beaconsmind.default.beaconContactsSummaries
            let list = beaconContactsSummary.map { summary -> NSDictionary in
                return [
                    "uuid": summary.uuid,
                    "major": summary.major,
                    "minor": summary.minor,
                    "name": summary.name,
                    "store": summary.store,
                    "currentRSSI": summary.rssi,
                    "averageRSSI": summary.averageRSSI,
                    "lastContactTimestamp": summary.timestamp,
                    "contactFrequencyPerMinute": summary.frequency,
                    "contactsCount": summary.contactCount,
                    "isInRange": summary.isInRange,
                ]
            }
            resolve(list)
        } catch {
            reject("500", "getBeaconsSummary failed", error)
        }
    }
    
    @objc(setMinLogLevel:withResolver:withRejecter:)
    func setMinLogLevel(options: NSDictionary, resolve: @escaping RCTPromiseResolveBlock, reject: @escaping RCTPromiseRejectBlock) -> Void {
        guard let args = options as? [String: Any],
              let minLogLevelString = args["minLogLevel"] as? String
        else {
            reject("500", "Invalid arguments", nil)
            return
        }
        
        let minLogLevel = LogLevel.init(rawValue: minLogLevelString) ?? LogLevel.silent
        Beaconsmind.default.setMinLogLevel(level: minLogLevel)
        
        resolve(true)
    }

    private func onMainThread(_ closure: @escaping () -> Void) {
        if Thread.isMainThread {
            closure()
        } else {
            DispatchQueue.main.async {
                closure()
            }
        }
    }

    private func getDateDayFromSeconds(seconds: Double?) -> DateDay? {
        var d: DateDay? = nil
        if seconds != nil {
            d = DateDay(date: Date(timeIntervalSince1970: seconds! ))
        }
        return d;
    }
}

extension BeaconsmindReactNativeSDK: BeaconListenerDelegate {
    func proximity(inRegion: CLBeaconRegion) {
        debugPrint("proximity inRegion", inRegion)
    }

    func proximity(leftRegion: CLBeaconRegion) {
        debugPrint("proximity leftRegion", leftRegion)
    }

    public func ranged(beacons: [CLBeacon]) {
        debugPrint("ranged", beacons)
    }

    public func ranged(region: CLRegion?, error: Error) {
        debugPrint("ranged error", error)
    }
}
