module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        alias: {
          '@navigation': './src/navigation',
          '@screens': './src/screens',
          '@features': './src/features',
          '@components': './src/components',
          '@theme': './src/theme',
          '@hooks': './src/hooks',
          '@utils': './src/utils',
        },
      },
    ],
  ],
};
