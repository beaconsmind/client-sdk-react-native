const { getDefaultConfig, mergeConfig } = require('@react-native/metro-config');
const path = require('path');

const sdkPath = path.resolve(`${__dirname}/..`);

/**
 * Metro configuration
 * https://facebook.github.io/metro/docs/configuration
 *
 * @type {import('metro-config').MetroConfig}
 */
const config = {
  resolver: {
    extraNodeModules: {
      '@beaconsmind/react-native-sdk': sdkPath,
    },
  },
  watchFolders: [sdkPath],
};

module.exports = mergeConfig(getDefaultConfig(__dirname), config);
