package com.beaconsmind.reactdemo;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
//import com.moengage.firebase.MoEFireBaseMessagingService;
import com.wix.reactnativenotifications.fcm.FcmInstanceIdListenerService;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class FirebaseMessagingServiceProxy extends FirebaseMessagingService {
  private final List<FirebaseMessagingService> messagingServices = new ArrayList<FirebaseMessagingService>() {{
      add(new FcmInstanceIdListenerService());
//    add(new MoEFireBaseMessagingService());
  }};

  @Override
  public void onMessageReceived(@NonNull RemoteMessage message) {
    delegate(service -> {
      injectContext(service);
      service.onMessageReceived(message);
    });
  }

  @Override
  public void onNewToken(String s) {
    delegate(service -> {
      injectContext(service);
      service.onNewToken(s);
    });
  }

  @Override
  public void handleIntent(@NonNull Intent intent) {
    delegate(service -> {
      injectContext(service);
      service.handleIntent(intent);
    });
  }

  private void delegate(GCAction1<FirebaseMessagingService> action) {
    for (FirebaseMessagingService service : messagingServices) {
      action.run(service);
    }
  }

  private void injectContext(FirebaseMessagingService service) {
    setField(service, "mBase", this);
  }

  private void setField(Object targetObject, String fieldName, Object fieldValue) {
    Field field;
    try {
      field = targetObject.getClass().getDeclaredField(fieldName);
    } catch (NoSuchFieldException e) {
      field = null;
    }
    Class superClass = targetObject.getClass().getSuperclass();
    while (field == null && superClass != null) {
      try {
        field = superClass.getDeclaredField(fieldName);
      } catch (NoSuchFieldException e) {
        superClass = superClass.getSuperclass();
      }
    }
    if (field == null) {
      return;
    }
    field.setAccessible(true);
    try {
      field.set(targetObject, fieldValue);
    } catch (IllegalAccessException ignored) {
    }
  }
}

interface GCAction1<T> {
  void run(T t);
}

