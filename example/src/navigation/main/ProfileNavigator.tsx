import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Profile, ProfileDetails, ResetPassword } from '@screens/index';
import { NavigationProp } from '@react-navigation/native';

export type ScreenNames = ['Profile Details', 'Reset Password', 'Profile'];
export type StackParamList = Record<ScreenNames[number], undefined>;
export type StackNavigation = NavigationProp<StackParamList>;

const ProfileStackNavigator = createNativeStackNavigator<StackParamList>();

const ProfileNavigator = () => {
  return (
    <ProfileStackNavigator.Navigator>
      <ProfileStackNavigator.Screen name="Profile" component={Profile} />
      <ProfileStackNavigator.Screen
        name="Profile Details"
        component={ProfileDetails}
      />
      <ProfileStackNavigator.Screen
        name="Reset Password"
        component={ResetPassword}
      />
    </ProfileStackNavigator.Navigator>
  );
};

export default ProfileNavigator;
