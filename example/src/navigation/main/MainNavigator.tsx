import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Image, StyleSheet } from 'react-native';
import ProfileNavigator from './ProfileNavigator';
import OffersNavigator from './OffersNavigator';
import BeaconsNavigator from './BeaconsNavigator';

const TabNavigation = createBottomTabNavigator();

const ProfileImage = require('../../assets/profile.png');
const ProfileImageActive = require('../../assets/profile-active.png');

const BeaconsImage = require('../../assets/beacons.png');
const BeaconsImageActive = require('../../assets/beacons-active.png');

const OffersImage = require('../../assets/offers.png');
const OffersImageActive = require('../../assets/offers-active.png');

const ProfileIcon = ({ focused }: { focused: boolean }) => (
  <Image
    style={styles.image}
    source={focused ? ProfileImageActive : ProfileImage}
  />
);

const BeaconsIcon = ({ focused }: { focused: boolean }) => (
  <Image
    style={styles.imageBeacons}
    source={focused ? BeaconsImageActive : BeaconsImage}
  />
);

const OffersIcon = ({ focused }: { focused: boolean }) => (
  <Image
    style={styles.image}
    source={focused ? OffersImageActive : OffersImage}
  />
);

const MainNavigator = () => {
  return (
    <TabNavigation.Navigator screenOptions={{ headerShown: false }}>
      <TabNavigation.Screen
        name="Beacons tab"
        component={BeaconsNavigator}
        options={{ tabBarIcon: BeaconsIcon }}
      />
      <TabNavigation.Screen
        name="Offers tab"
        component={OffersNavigator}
        options={{ tabBarIcon: OffersIcon }}
      />
      <TabNavigation.Screen
        name="Profile tab"
        component={ProfileNavigator}
        options={{
          tabBarIcon: ProfileIcon,
        }}
      />
    </TabNavigation.Navigator>
  );
};

export default MainNavigator;

const styles = StyleSheet.create({
  image: {
    width: 20,
    height: 20,
  },
  imageBeacons: {
    width: 28,
    height: 28,
  },
});
