import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Offers, Offer } from '@screens/index';
import { NavigationProp } from '@react-navigation/native';

export type ScreenNames = ['Offers', 'Offer'];
export type StackParamList = Record<ScreenNames[number], any>;
export type StackNavigation = NavigationProp<StackParamList>;

const OffersStackNavigator = createNativeStackNavigator<StackParamList>();

const OffersNavigator = () => {
  return (
    <OffersStackNavigator.Navigator>
      <OffersStackNavigator.Screen name="Offers" component={Offers} />
      <OffersStackNavigator.Screen name="Offer" component={Offer} />
    </OffersStackNavigator.Navigator>
  );
};

export default OffersNavigator;
