import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Beacons, Offer } from '@screens/index';
import { NavigationProp } from '@react-navigation/native';

export type ScreenNames = ['Beacons', 'Offer'];
export type StackParamList = Record<ScreenNames[number], any>;
export type StackNavigation = NavigationProp<StackParamList>;

const BeaconsStackNavigator = createNativeStackNavigator<StackParamList>();

const BeaconsNavigator = () => {
  return (
    <BeaconsStackNavigator.Navigator>
      <BeaconsStackNavigator.Screen name="Beacons" component={Beacons} />
      <BeaconsStackNavigator.Screen name="Offer" component={Offer} />
    </BeaconsStackNavigator.Navigator>
  );
};

export default BeaconsNavigator;
