import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Login, Register, Import } from '@screens/index';
import { NavigationProp } from '@react-navigation/native';

export type ScreenNames = ['Register', 'Login', 'Import'];
export type StackParamList = Record<ScreenNames[number], undefined>;
export type StackNavigation = NavigationProp<StackParamList>;

const AuthStackNavigator = createNativeStackNavigator<StackParamList>();

const AuthNavigator = () => {
  return (
    <AuthStackNavigator.Navigator>
      <AuthStackNavigator.Screen name="Login" component={Login} />
      <AuthStackNavigator.Screen name="Register" component={Register} />
      <AuthStackNavigator.Screen name="Import" component={Import} />
    </AuthStackNavigator.Navigator>
  );
};

export default AuthNavigator;
