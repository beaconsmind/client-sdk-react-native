import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AuthNavigator from './auth/AuthNavigator';
import { useBeaconsmindContext } from '@features/beaconsmind/Context';
import MainNavigator from './main/MainNavigator';

const Navigation = () => {
  const { user } = useBeaconsmindContext();

  return (
    <NavigationContainer>
      {user ? <MainNavigator /> : <AuthNavigator />}
    </NavigationContainer>
  );
};

export default Navigation;
