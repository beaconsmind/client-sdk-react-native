import React from 'react';

import Navigation from './navigation';
import { BeaconsmindProvider } from '@features/beaconsmind/Context';

function App(): JSX.Element {
  return (
    <BeaconsmindProvider>
      <Navigation />
    </BeaconsmindProvider>
  );
}

export default App;
