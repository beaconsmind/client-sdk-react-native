const colors = {
  black: '#000000',
  white: '#FFFFFF',
  blue: '#6699CC',
  grey: '#808080',
  red: '#FF0000',
  green: '#00FF00',
};

export default colors;
