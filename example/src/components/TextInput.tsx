import colors from '@theme/colors';
import React from 'react';
import { TextInput as RNTextInput, StyleSheet, Text, View } from 'react-native';

export type OnChangeFnType = (e: any) => void;

type TextInputProps = {
  value: string;
  onChange: OnChangeFnType;
  title: string;
  onFocus?: (e: any) => void;
  isPassword?: boolean;
  placeholder?: string;
  autoCapitalize?: 'none' | 'sentences' | 'words' | 'characters';
  disabled?: boolean;
};
const TextInput = ({
  value,
  onChange,
  title,
  onFocus,
  isPassword,
  placeholder,
  autoCapitalize,
  disabled,
}: TextInputProps) => {
  return (
    <View>
      <Text style={styles.text}>{title}</Text>
      <RNTextInput
        style={styles.input}
        value={value}
        onChangeText={onChange}
        onFocus={onFocus}
        secureTextEntry={isPassword}
        placeholder={placeholder}
        autoCapitalize={autoCapitalize}
        editable={!disabled}
      />
    </View>
  );
};

export default TextInput;

const styles = StyleSheet.create({
  input: {
    height: 40,
    width: '100%',
    borderWidth: 1,
    borderColor: colors.black,
    borderRadius: 2,
    paddingLeft: 10,
  },
  container: {
    display: 'flex',
    gap: 2,
  },
  text: {
    fontSize: 10,
  },
});
