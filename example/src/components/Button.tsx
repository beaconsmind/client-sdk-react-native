import colors from '@theme/colors';
import React from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native';

type ButtonProps = {
  text: string;
  onPress: (e: any) => void;
  type?: 'primary' | 'secondary' | 'dark';
  disabled?: boolean;
};

const Button = ({ text, onPress, type = 'primary', disabled }: ButtonProps) => (
  <TouchableOpacity
    disabled={disabled}
    style={[styles[type], disabled ? styles.disabled : undefined]}
    onPress={onPress}>
    <Text style={styles[`text${type}`]}>{text}</Text>
  </TouchableOpacity>
);

export default Button;

const styles = StyleSheet.create({
  primary: {
    backgroundColor: colors.blue,
    borderRadius: 2,
    width: '100%',
    height: 42,
    alignItems: 'center',
    justifyContent: 'center',
  },
  secondary: {
    backgroundColor: colors.white,
    borderRadius: 2,
    width: '100%',
    borderColor: colors.blue,
    borderWidth: 1,
    height: 42,
    alignItems: 'center',
    justifyContent: 'center',
  },
  dark: {
    backgroundColor: colors.grey,
    borderRadius: 2,
    width: '100%',
    height: 42,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textprimary: {
    color: colors.white,
  },
  textsecondary: {
    color: colors.blue,
  },
  textdark: {
    color: colors.black,
  },
  disabled: { opacity: 0.5 },
});
