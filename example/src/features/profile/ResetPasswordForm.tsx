import React, { useState } from 'react';
import { TextInput, Button } from '@components/index';
import { Alert, ScrollView, StyleSheet } from 'react-native';
import { useBeaconsmindContext } from '@features/beaconsmind/Context';

const ResetPasswordForm = () => {
  const { changePassword } = useBeaconsmindContext();

  const [oldPassword, setOldPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');

  const [disableButton, setDisableButton] = useState(false);

  const handleReset = () => {
    if (oldPassword && newPassword && confirmPassword) {
      if (newPassword !== confirmPassword) {
        Alert.alert('Passwords do not match');
      } else {
        setDisableButton(true);
        changePassword({ oldPassword, newPassword }, () =>
          setDisableButton(false),
        );
      }
    } else {
    }
  };

  return (
    <ScrollView style={styles.container} contentContainerStyle={styles.content}>
      <TextInput
        title="Old password"
        placeholder="Old password"
        isPassword
        value={oldPassword}
        onChange={value => setOldPassword(value)}
      />
      <TextInput
        title="New password"
        placeholder="password"
        isPassword
        value={newPassword}
        onChange={value => setNewPassword(value)}
      />
      <TextInput
        title="Confirm password"
        placeholder="password"
        isPassword
        value={confirmPassword}
        onChange={value => setConfirmPassword(value)}
      />
      <Button text="UPDATE" onPress={handleReset} disabled={disableButton} />
    </ScrollView>
  );
};

export default ResetPasswordForm;

const styles = StyleSheet.create({
  container: {
    width: '80%',
    paddingTop: 40,
  },
  content: {
    justifyContent: 'center',
    alignContent: 'center',
    gap: 20,
  },
});
