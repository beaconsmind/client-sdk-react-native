import React, { useState } from 'react';
import { TextInput, Button } from '@components/index';
import { ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native';
import { useBeaconsmindContext } from '@features/beaconsmind/Context';
import DatePicker from 'react-native-date-picker';

const ProfileDetailsForm = () => {
  const { beaconsmindUser, updateProfile } = useBeaconsmindContext();

  const [firstName, setFirstName] = useState(beaconsmindUser?.firstName || '');
  const [lastName, setLastName] = useState(beaconsmindUser?.lastName || '');
  const [email, setEmail] = useState(beaconsmindUser?.userName || '');
  const [street, setStreet] = useState(beaconsmindUser?.street || '');
  const [city, setCity] = useState(beaconsmindUser?.city || '');
  const [country, setCountry] = useState(beaconsmindUser?.country || '');
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [language, setLanguage] = useState(
    (beaconsmindUser as any).language || '',
  );
  const [dob, setDob] = useState(
    beaconsmindUser?.birthDate
      ? new Date(beaconsmindUser.birthDate).toDateString()
      : '',
  );

  const [disableButton, setDisableButton] = useState(false);
  const handleUpdate = () => {
    setDisableButton(true);
    updateProfile(
      {
        firstName,
        lastName,
        userName: email,
        street,
        city,
        country,
        language,
        birthDateSeconds: dob ? new Date(dob).getTime() / 1000 : undefined,
      },
      () => setDisableButton(false),
    );
  };

  return (
    <ScrollView style={styles.container} contentContainerStyle={styles.content}>
      <TextInput
        title="First Name"
        placeholder="First Name"
        value={firstName}
        onChange={setFirstName}
      />
      <TextInput
        title="Last Name"
        placeholder="Last Name"
        value={lastName}
        onChange={setLastName}
      />
      <TextInput
        title="Email"
        placeholder="Email"
        value={email}
        onChange={setEmail}
        autoCapitalize="none"
        disabled
      />

      <TextInput
        title="Street"
        placeholder="Street"
        value={street}
        onChange={setStreet}
      />
      <TextInput
        title="City"
        placeholder="City"
        value={city}
        onChange={setCity}
      />
      <TextInput
        title="Country"
        placeholder="Country"
        value={country}
        onChange={setCountry}
      />
      <TextInput
        title="Language"
        placeholder="Language"
        value={language}
        onChange={setLanguage}
      />
      <TouchableOpacity onPress={() => setShowDatePicker(true)}>
        <View style={styles.inputWrapper}>
          <TextInput
            title="Date of birth"
            placeholder="dd/MM/yyyy"
            value={dob ? new Date(dob)?.toLocaleDateString('en-US') : ''}
            onChange={setDob}
            disabled
          />
        </View>
      </TouchableOpacity>
      <DatePicker
        date={dob ? new Date(dob) : new Date()}
        modal
        mode="date"
        onCancel={() => setShowDatePicker(false)}
        open={showDatePicker}
        onConfirm={date => {
          setDob(date.toString());
          setShowDatePicker(false);
        }}
      />

      <Button text="UPDATE" onPress={handleUpdate} disabled={disableButton} />
    </ScrollView>
  );
};

export default ProfileDetailsForm;

const styles = StyleSheet.create({
  container: {
    width: '80%',
    paddingTop: 40,
  },
  content: {
    justifyContent: 'center',
    alignContent: 'center',
    gap: 20,
  },
  inputWrapper: {
    pointerEvents: 'none',
  },
});
