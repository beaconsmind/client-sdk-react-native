import React from 'react';
import { useBeaconsmindContext } from '@features/beaconsmind/Context';
import { Platform, Text, View } from 'react-native';
const { version } = require('@beaconsmind/react-native-sdk/package.json');

const AppInfo = () => {
  const { hostname } = useBeaconsmindContext();
  return (
    <View>
      <Text>ReactNative SDK: v{version}</Text>
      <Text>{Platform.OS} SDK: unknown</Text>
      <Text>Host: {hostname}</Text>
    </View>
  );
};

export default AppInfo;
