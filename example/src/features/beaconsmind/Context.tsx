import React, {
  createContext,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import * as Beaconsmind from '@beaconsmind/react-native-sdk';
import { ImportAccountOptions } from '@beaconsmind/react-native-sdk';
import { Alert } from 'react-native';
import { BACKEND_URLS } from '@utils/constants';

type ProviderProps = {
  children: React.ReactNode;
};

type ImportAccountProps = ImportAccountOptions & {
  onSettled?: () => void;
};

type RegisterProps = {
  firstName: string;
  lastName: string;
  password: string;
  confirmPassword: string;
  email: string;
  onSettled?: () => void;
};

type LoginProps = {
  password: string;
  email: string;
  onSettled?: () => void;
};

type UserProfile = {
  id: string;
  imported: boolean;
  email: string;
  firstName: string;
  lastName: string;
};

type ContextType = {
  user: null | UserProfile;
  register: (props: RegisterProps) => Promise<void>;
  importAccount: (props: ImportAccountProps) => Promise<void>;
  updateProfile: (props: any, onSettled: () => void) => Promise<void>;
  changePassword: (props: any, onSettled: () => void) => Promise<void>;
  login: (props: LoginProps) => Promise<void>;
  logout: () => Promise<void>;
  hostname: string;
  setHostname: (hostname: string) => void;
  usePassiveScanning: boolean;
  setUsePassiveScanning: (usePassiveScanning: boolean) => void;
  init: (hostname: string) => Promise<void>;
  minLogLevel?: Beaconsmind.LogLevel;
  setMinLogLevel: (_logLevel: Beaconsmind.LogLevel) => Promise<void>;
};

const defaultValue: ContextType = {
  user: null,
  logout: () => Promise.resolve(),
  register: () => Promise.resolve(),
  importAccount: () => Promise.resolve(),
  updateProfile: () => Promise.resolve(),
  changePassword: () => Promise.resolve(),
  login: () => Promise.resolve(),
  hostname: BACKEND_URLS[0],
  setHostname: () => null,
  usePassiveScanning: true,
  setUsePassiveScanning: () => null,
  minLogLevel: undefined,
  setMinLogLevel: () => Promise.resolve(),
  init: () => Promise.resolve(),
};

const BeaconsmindContext = createContext(defaultValue);

const BeaconsmindProvider = ({ children }: ProviderProps) => {
  const [profile, setProfile] = useState<null | UserProfile>(null);
  const [hostname, setHostname] = useState(BACKEND_URLS[0]);
  const [usePassiveScanning, setUsePassiveScanning] = useState(true);
  const [minLogLevel, setMinLogLevelState] = useState<Beaconsmind.LogLevel>(
    Beaconsmind.LogLevel.debug,
  );

  useEffect(() => {
    Beaconsmind.getOAuthContext().then(response => {
      if (response?.userId) {
        console.log('Detected logged in user ' + response.userId);
        Beaconsmind.getProfile()
          // eslint-disable-next-line @typescript-eslint/no-shadow
          .then(profile => {
            setProfile({
              imported: false,
              id: profile.id,
              email: profile.userName,
              firstName: profile.firstName,
              lastName: profile.lastName,
            });
          })
          .catch(() => {
            console.log('User is anonymous, most likely imported');
            setProfile({
              imported: true,
              id: response.userId,
              email: 'unknown',
              firstName: 'unknown',
              lastName: 'unknown',
            });
          });
      } else {
        console.log('Logged in user not found');
      }
    });
  }, []);

  useEffect(() => {
    console.log('Changing SKD log level to ' + minLogLevel);
    Beaconsmind.setMinLogLevel(minLogLevel);
  }, [minLogLevel]);

  const init = useCallback(
    async (host = BACKEND_URLS[0]) => {
      await Beaconsmind.initialize({
        hostname: host,
        appVersion: '0.0.1',
        platformOptions: {
          android: {
            usePassiveScanning,
            notification: {
              notificationBadgeName: 'ic_beacons',
              notificationChannelName: 'beaconsmind',
              notificationTitle: 'Beaconsmind sdk demo',
              notificationText: 'Listening to beacons',
            },
          },
        },
      });
    },
    [usePassiveScanning],
  );

  useEffect(() => {
    init();
  }, [init]);

  const login = useCallback(async (props: LoginProps) => {
    try {
      await Beaconsmind.login(props.email, props.password);
      props.onSettled && props.onSettled();
      const userProfile = await Beaconsmind.getProfile();
      setProfile({
        imported: false,
        id: userProfile.id,
        email: userProfile.userName,
        firstName: userProfile.firstName,
        lastName: userProfile.lastName,
      });
    } catch {
      Alert.alert('Wrong credentials');
      props.onSettled && props.onSettled();
    }
  }, []);

  const register = useCallback(
    async ({
      firstName,
      lastName,
      password,
      confirmPassword,
      email,
      onSettled,
    }: RegisterProps) => {
      try {
        await Beaconsmind.signup({
          username: email,
          firstName,
          lastName,
          password,
          confirmPassword,
        });
        onSettled && onSettled();
        // eslint-disable-next-line @typescript-eslint/no-shadow
        const profile = await Beaconsmind.getProfile();
        setProfile({
          imported: false,
          id: profile.id,
          email: profile.userName,
          firstName: profile.firstName,
          lastName: profile.lastName,
        });
      } catch {
        Alert.alert('Something went wrong');
        onSettled && onSettled();
      }
    },
    [setProfile],
  );

  const importAccount = useCallback(
    async ({ onSettled, ...request }: ImportAccountProps) => {
      try {
        const response = await Beaconsmind.importAccount(request);
        if (response?.userId) {
          setProfile({
            imported: true,
            id: response.userId,
            firstName: request.firstName || '',
            lastName: request.lastName || '',
            email: request.email,
          });
        } else {
          Alert.alert('Failed to import account, got null user id');
        }
      } catch (ex: any) {
        Alert.alert('Something went wrong', ex.toString());
      }
      onSettled && onSettled();
    },
    [setProfile],
  );

  const logout = useCallback(async () => {
    try {
      await Beaconsmind.logout();
      setProfile(null);
    } catch {
      Alert.alert('Something went wrong');
    }
  }, []);

  const updateProfile = useCallback(
    async (fields: any, onSettled: () => void) => {
      try {
        const updated = await Beaconsmind.updateProfile(fields);
        setProfile({
          imported: false,
          id: updated.id,
          firstName: updated.firstName,
          lastName: updated.lastName,
          email: updated.userName,
        });
        onSettled && onSettled();
        Alert.alert('Profile updated');
      } catch {
        onSettled && onSettled();
        Alert.alert('Something went wrong');
      }
    },
    [],
  );

  const changePassword = useCallback(
    async (fields: any, onSettled: () => void) => {
      try {
        await Beaconsmind.changePassword(fields);
        onSettled && onSettled();
        Alert.alert('Password reset');
      } catch {
        onSettled && onSettled();
        Alert.alert('Something went wrong');
      }
    },
    [],
  );

  const setMinLogLevel = useCallback(async (logLevel: Beaconsmind.LogLevel) => {
    setMinLogLevelState(logLevel);
    await Beaconsmind.setMinLogLevel(logLevel);
  }, []);

  const value = useMemo<ContextType>(
    () => ({
      user: profile,
      register,
      login,
      logout,
      hostname,
      setHostname,
      init,
      updateProfile,
      changePassword,
      usePassiveScanning,
      setUsePassiveScanning,
      minLogLevel,
      setMinLogLevel,
      importAccount,
    }),
    [
      profile,
      register,
      login,
      logout,
      hostname,
      setHostname,
      init,
      updateProfile,
      changePassword,
      usePassiveScanning,
      setUsePassiveScanning,
      minLogLevel,
      setMinLogLevel,
      importAccount,
    ],
  );

  return (
    <BeaconsmindContext.Provider value={value}>
      {children}
    </BeaconsmindContext.Provider>
  );
};

function useBeaconsmindContext() {
  const context = React.useContext(BeaconsmindContext);
  if (context === undefined) {
    throw new Error('useCount must be used within a CountProvider');
  }
  return context;
}

export { useBeaconsmindContext, BeaconsmindProvider };
