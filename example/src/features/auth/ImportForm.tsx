import React, { useState } from 'react';
import { TextInput, Button } from '@components/index';
import { Alert, ScrollView, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { StackNavigation } from '@navigation/auth/AuthNavigator';
import { useBeaconsmindContext } from '@features/beaconsmind/Context';
import { validateEmail } from '@utils/validators';

const ImportForm = () => {
  const [id, setId] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [disableButton, setDisableButton] = useState(false);
  const navigation = useNavigation<StackNavigation>();
  const { importAccount } = useBeaconsmindContext();

  const handleImport = async () => {
    if (firstName && lastName && email && id) {
      if (!validateEmail(email)) {
        Alert.alert('Wrong email format');
      } else {
        setDisableButton(true);
        await importAccount({
          id,
          email,
          firstName,
          lastName,
          gender: undefined,
          language: undefined,
          birthDateSeconds: undefined,
          onSettled: () => setDisableButton(false),
        });
      }
    } else {
      Alert.alert('All fields are required');
    }
  };

  return (
    <ScrollView style={styles.container} contentContainerStyle={styles.content}>
      <TextInput
        title="Id"
        placeholder="Id"
        value={id}
        onChange={setId}
        autoCapitalize="none"
      />
      <TextInput
        title="Email"
        placeholder="Email"
        value={email}
        onChange={setEmail}
        autoCapitalize="none"
      />
      <TextInput
        title="First Name"
        placeholder="First Name"
        value={firstName}
        onChange={setFirstName}
      />
      <TextInput
        title="Last Name"
        placeholder="Last Name"
        value={lastName}
        onChange={setLastName}
      />
      <Button text="IMPORT" onPress={handleImport} disabled={disableButton} />
      <Button
        type="secondary"
        text="Sign In instead"
        onPress={() => navigation.navigate('Login')}
      />
    </ScrollView>
  );
};

export default ImportForm;

const styles = StyleSheet.create({
  container: {
    width: '80%',
    paddingTop: 40,
  },
  content: {
    justifyContent: 'center',
    alignContent: 'center',
    gap: 20,
  },
});
