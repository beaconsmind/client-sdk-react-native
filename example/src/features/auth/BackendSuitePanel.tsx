import colors from '@theme/colors';
import React, { useCallback, useState } from 'react';
import { StyleSheet, View, Modal, Switch, Text, Platform } from 'react-native';
import { Button, TextInput } from '@components/index';
import RadioButtonsGroup from 'react-native-radio-buttons-group';
import { BACKEND_URLS } from '@utils/constants';
import { useBeaconsmindContext } from '@features/beaconsmind/Context';
import { LogLevel } from '@beaconsmind/react-native-sdk';

const BackendSuitePanel = ({}) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isLogLevelVisible, setIsLogLevelVisible] = useState(false);
  const {
    init,
    hostname,
    setHostname,
    usePassiveScanning,
    setUsePassiveScanning,
    minLogLevel,
    setMinLogLevel,
  } = useBeaconsmindContext();

  const predefinedHostname =
    BACKEND_URLS.find(url => url === hostname) || 'custom';
  const setPredefinedHostname = (value: string) => {
    if (value === 'custom') {
      setHostname('https://');
    } else {
      setHostname(value);
    }
  };

  const closeAndInitSdk = useCallback(() => {
    console.log(`Initializing SDK with ${hostname}`);
    setIsModalVisible(false);
    init(hostname);
  }, [init, hostname]);

  return (
    <View style={styles.container}>
      <Button
        text={
          Platform.OS === 'android'
            ? 'BACKEND SERVER / PASSIVE SCANNING'
            : 'BACKEND SERVER'
        }
        onPress={() => setIsModalVisible(true)}
        type="dark"
      />
      <Modal
        visible={isModalVisible}
        onRequestClose={closeAndInitSdk}
        onDismiss={closeAndInitSdk}>
        <View style={styles.modal}>
          <RadioButtonsGroup
            radioButtons={[
              ...BACKEND_URLS.map(url => ({ label: 'Use ' + url, id: url })),
              { id: 'custom', label: 'Use custom hostname' },
            ]}
            onPress={setPredefinedHostname}
            selectedId={predefinedHostname}
          />
          <View style={styles.input}>
            <TextInput
              title="Hostname"
              value={hostname}
              onChange={setHostname}
            />
          </View>
          {Platform.OS === 'android' && (
            <View style={styles.passiveScanningWrapper}>
              <Text>Passive scanning</Text>
              <Switch
                value={usePassiveScanning}
                onValueChange={value => {
                  console.log(
                    value
                      ? 'Turning on passive scanning'
                      : 'Turning off passive scanning',
                  );
                  setUsePassiveScanning(value);
                }}
              />
            </View>
          )}
          <Button
            text={
              'Log Level' +
              (minLogLevel ? ' (' + minLogLevel.toUpperCase() + ')' : '')
            }
            type="secondary"
            onPress={() => setIsLogLevelVisible(true)}
          />
          <Button text="CLOSE" type="secondary" onPress={closeAndInitSdk} />
        </View>
      </Modal>
      <Modal
        visible={isLogLevelVisible}
        onRequestClose={() => setIsLogLevelVisible(false)}>
        <RadioButtonsGroup
          containerStyle={styles.logLevels}
          radioButtons={[
            { id: LogLevel.debug, label: 'Debug' },
            { id: LogLevel.info, label: 'Info' },
            { id: LogLevel.warning, label: 'Warning' },
            { id: LogLevel.error, label: 'Error' },
            { id: LogLevel.silent, label: 'Silent' },
          ]}
          onPress={async id => {
            await setMinLogLevel(id as LogLevel);
            setIsLogLevelVisible(false);
          }}
          selectedId={minLogLevel}
        />
      </Modal>
    </View>
  );
};

export default BackendSuitePanel;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 10,
  },
  text: {
    color: colors.black,
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    gap: 30,
    width: '100%',
    paddingHorizontal: 30,
  },
  logLevels: {
    alignItems: 'flex-start',
    paddingLeft: 30,
    paddingTop: 30,
  },
  input: {
    width: '100%',
  },
  passiveScanningWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 6,
  },
});
