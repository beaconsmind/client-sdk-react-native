import React, { useState } from 'react';
import { TextInput, Button } from '@components/index';
import { Alert, ScrollView, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { StackNavigation } from '@navigation/auth/AuthNavigator';
import { useBeaconsmindContext } from '@features/beaconsmind/Context';
import { validateEmail } from '@utils/validators';

const RegisterForm = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [disableButton, setDisableButton] = useState(false);
  const navigation = useNavigation<StackNavigation>();

  const { register } = useBeaconsmindContext();

  const handleRegister = async () => {
    if (firstName && lastName && email && password && confirmPassword) {
      if (!validateEmail(email)) {
        Alert.alert('Wrong email format');
      } else if (password !== confirmPassword) {
        Alert.alert('Passwords do not match');
      } else {
        setDisableButton(true);
        await register({
          firstName,
          lastName,
          email,
          password,
          confirmPassword,
          onSettled: () => setDisableButton(false),
        });
      }
    } else {
      Alert.alert('All fields are required');
    }
  };

  return (
    <ScrollView style={styles.container} contentContainerStyle={styles.content}>
      <TextInput
        title="First Name"
        placeholder="First Name"
        value={firstName}
        onChange={value => setFirstName(value)}
      />
      <TextInput
        title="Last Name"
        placeholder="Last Name"
        value={lastName}
        onChange={value => setLastName(value)}
      />
      <TextInput
        title="Email"
        placeholder="Email"
        value={email}
        onChange={value => setEmail(value)}
        autoCapitalize="none"
      />

      <TextInput
        isPassword
        title="Password"
        placeholder="Password"
        value={password}
        onChange={value => setPassword(value)}
      />
      <TextInput
        isPassword
        title="Confirm password"
        placeholder="Confirm password"
        value={confirmPassword}
        onChange={value => setConfirmPassword(value)}
      />
      <Button
        text="REGISTER"
        onPress={handleRegister}
        disabled={disableButton}
      />
      <Button
        type="secondary"
        text="Sign In instead"
        onPress={() => navigation.navigate('Login')}
      />
    </ScrollView>
  );
};

export default RegisterForm;

const styles = StyleSheet.create({
  container: {
    width: '80%',
    paddingTop: 40,
  },
  content: {
    justifyContent: 'center',
    alignContent: 'center',
    gap: 20,
  },
});
