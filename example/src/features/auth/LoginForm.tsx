import React, { useState } from 'react';
import { TextInput, Button } from '@components/index';
import { Alert, ScrollView, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { StackNavigation } from '@navigation/auth/AuthNavigator';
import BackendSuitePanel from './BackendSuitePanel';
import { useBeaconsmindContext } from '@features/beaconsmind/Context';

const LoginForm = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [disableButton, setDisableButton] = useState(false);
  const navigation = useNavigation<StackNavigation>();

  const { login } = useBeaconsmindContext();

  const handleLogin = () => {
    if (email && password) {
      setDisableButton(true);
      login({ email, password, onSettled: () => setDisableButton(false) });
    } else {
      Alert.alert('All fields are required');
    }
  };

  return (
    <ScrollView style={styles.container} contentContainerStyle={styles.content}>
      <TextInput
        title="Email"
        placeholder="Email"
        value={email}
        onChange={value => setEmail(value)}
        autoCapitalize="none"
      />
      <TextInput
        title="Password"
        placeholder="Password"
        value={password}
        isPassword
        onChange={value => setPassword(value)}
      />
      <Button text="Login" disabled={disableButton} onPress={handleLogin} />
      <Button
        type="secondary"
        text="Import Account"
        onPress={() => navigation.navigate('Import')}
      />
      <Button
        type="secondary"
        text="Create Account"
        onPress={() => navigation.navigate('Register')}
      />
      <BackendSuitePanel />
    </ScrollView>
  );
};

export default LoginForm;

const styles = StyleSheet.create({
  container: {
    width: '80%',
    paddingTop: 40,
  },
  content: {
    justifyContent: 'center',
    alignContent: 'center',
    gap: 20,
  },
});
