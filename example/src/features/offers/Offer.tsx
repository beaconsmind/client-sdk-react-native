import React from 'react';
import { Offer } from '@beaconsmind/react-native-sdk';
import { Image, Pressable, StyleSheet, Text } from 'react-native';

const OfferItem = ({ item, onPress }: { item: Offer; onPress: () => void }) => {
  return (
    <Pressable style={styles.container} onPress={onPress}>
      <Text style={styles.title}>{item.title}</Text>
      <Image source={{ uri: item.imageUrl }} style={styles.image} />
    </Pressable>
  );
};

export default OfferItem;

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    padding: 16,
    borderRadius: 4,
    display: 'flex',
    gap: 3,
    marginBottom: 10,
    width: '100%',
  },
  title: {
    fontSize: 20,
    width: '100%',
    textAlign: 'center',
  },
  image: {
    height: 400,
    width: 'auto',
    resizeMode: 'contain',
  },
});
