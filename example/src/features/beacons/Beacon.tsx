import React from 'react';
import { BeaconContactSummary } from '@beaconsmind/react-native-sdk';
import { StyleSheet, Text, View } from 'react-native';
import colors from '@theme/colors';

const Beacon = ({ item }: { item: BeaconContactSummary }) => (
  <View
    style={[
      styles.container,
      item.isInRange ? styles.active : styles.inactive,
    ]}>
    <View style={styles.row}>
      <Text style={styles.bold}>Name:</Text>
      <Text>{item.name}</Text>
    </View>
    <View style={styles.row}>
      <Text style={styles.bold}>Store:</Text>
      <Text>{item.store}</Text>
    </View>
    <View style={styles.row}>
      <Text style={styles.bold}>Last contact:</Text>
      <Text>
        {item.lastContactTimestamp
          ? new Date(item.lastContactTimestamp).toLocaleDateString()
          : 'Never contacted'}
      </Text>
    </View>
    {item.isInRange && (
      <View style={styles.row}>
        <Text style={styles.bold}>Signal:</Text>
        <Text>{item.averageRSSI?.toFixed(2)}</Text>
      </View>
    )}
  </View>
);

export default Beacon;

const styles = StyleSheet.create({
  container: {
    borderWidth: 2,
    padding: 16,
    borderRadius: 2,
    display: 'flex',
    gap: 3,
    marginBottom: 10,
    width: '100%',
  },
  active: {
    borderColor: colors.green,
  },
  inactive: {
    borderColor: colors.black,
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    gap: 2,
  },
  bold: {
    fontWeight: 'bold',
  },
});
