import { checkPermission } from '@utils/permission';
import { useCallback, useEffect, useState } from 'react';
import { Platform } from 'react-native';
import { PERMISSIONS } from 'react-native-permissions';
import useCheckAppState from './useCheckAppState';

const useLocationPermission = () => {
  const [enabled, setEnabled] = useState<undefined | boolean>(undefined);

  const check = useCallback((checkOnly?: boolean) => {
    checkPermission({
      permission:
        Platform.OS === 'ios'
          ? PERMISSIONS.IOS.LOCATION_ALWAYS
          : PERMISSIONS.ANDROID.ACCESS_BACKGROUND_LOCATION,
      onEnabled: () => setEnabled(true),
      onDisabled: () => setEnabled(false),
      permissionName:
        Platform.OS === 'ios' ? 'location always' : 'background location',
      checkOnly,
    });
  }, []);

  useCheckAppState({ toActive: () => check(true) });

  useEffect(() => {
    check();
  }, [check]);

  return { enabled };
};

export default useLocationPermission;
