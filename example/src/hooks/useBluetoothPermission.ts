import { useCallback, useEffect, useState } from 'react';
import { Platform } from 'react-native';
import { PERMISSIONS } from 'react-native-permissions';
import DeviceInfo from 'react-native-device-info';
import { checkPermission } from '@utils/permission';
import useCheckAppState from './useCheckAppState';

const useBluetoothPermission = () => {
  const [enabled, setEnabled] = useState<undefined | boolean>(undefined);

  const check = useCallback((checkOnly?: boolean) => {
    checkPermission({
      permission:
        Platform.OS === 'ios'
          ? PERMISSIONS.IOS.BLUETOOTH_PERIPHERAL
          : PERMISSIONS.ANDROID.BLUETOOTH_SCAN,
      onEnabled: () => setEnabled(true),
      onDisabled: () => setEnabled(false),
      permissionName: 'bluetooth',
      checkOnly,
    });
  }, []);

  useCheckAppState({ toActive: () => check(true) });

  useEffect(() => {
    (async () => {
      const sdkLevel = await DeviceInfo.getApiLevel();
      if (Platform.OS === 'ios' || (sdkLevel && sdkLevel > 29)) {
        check();
      } else {
        setEnabled(true);
      }
    })();
  }, [setEnabled, check]);
  return { enabled };
};

export default useBluetoothPermission;
