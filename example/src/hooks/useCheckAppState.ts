import { useEffect, useRef } from 'react';
import { AppState } from 'react-native';

const useCheckAppState = ({ toActive }: { toActive?: () => void }) => {
  const appState = useRef(AppState.currentState);

  useEffect(() => {
    const subscription = AppState.addEventListener('change', nextAppState => {
      if (
        appState.current.match(/inactive|background/) &&
        nextAppState === 'active'
      ) {
        toActive && toActive();
      }

      appState.current = nextAppState;
    });

    return () => {
      subscription.remove();
    };
  }, [toActive]);
};

export default useCheckAppState;
