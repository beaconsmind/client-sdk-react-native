import { useEffect } from 'react';
import {
  RESULTS,
  checkNotifications,
  requestNotifications,
} from 'react-native-permissions';

const useNotificationPermissions = () => {
  useEffect(() => {
    checkNotifications().then(({ status }) => {
      switch (status) {
        case RESULTS.DENIED:
          requestNotifications([
            'alert',
            'criticalAlert',
            'badge',
            'sound',
            'provisional',
          ]);
          break;
        case RESULTS.BLOCKED:
          requestNotifications([
            'alert',
            'criticalAlert',
            'badge',
            'sound',
            'provisional',
          ]);
      }
    });
  }, []);
};

export default useNotificationPermissions;
