import {
  Notification,
  Notifications,
  Registered,
} from 'react-native-notifications';
import * as Beaconsmind from '@beaconsmind/react-native-sdk';

// Using a patch for react-native-notifications (patches/react-native-notifications+5.0.0.patch).
// Remove the patch and upgrade to new version of package once the fix for this issue is out
// https://github.com/wix/react-native-notifications/issues/975

const parseOfferId = (offerId: string) => parseInt(offerId, 10);

export const handleNotifications = ({
  onOpen,
}: {
  onOpen: (offerId: number) => void;
}) => {
  Notifications.getInitialNotification().then((notification?: Notification) => {
    if (notification) {
      const offerId = parseOfferId(notification.payload.offerId);
      onOpen(offerId);
    }
  });

  Notifications.events().registerRemoteNotificationsRegistered(
    (event: Registered) => {
      Beaconsmind.registerDeviceToken(event.deviceToken);
    },
  );

  Notifications.isRegisteredForRemoteNotifications().then(isRegistered => {
    if (!isRegistered) {
      Notifications.registerRemoteNotifications();
    }
  });

  const foregroundEmitter =
    Notifications.events().registerNotificationReceivedForeground(
      (notification: Notification) => {
        Beaconsmind.markOfferAsReceived(
          parseOfferId(notification.payload.offerId),
        );
        onOpen(parseOfferId(notification.payload.offerId));
      },
    );

  const backgroundEmitter =
    Notifications.events().registerNotificationReceivedBackground(
      (notification: Notification, completion: (response: any) => void) => {
        Beaconsmind.markOfferAsReceived(
          parseOfferId(notification.payload.offerId),
        );
        completion({ alert: true, sound: true, badge: false });
      },
    );

  const openedEmitter = Notifications.events().registerNotificationOpened(
    (notification: Notification, completion) => {
      onOpen(parseInt(notification.payload.offerId, 10));
      Beaconsmind.markOfferAsReceived(
        parseOfferId(notification.payload.offerId),
      );

      completion();
    },
  );

  return () => {
    openedEmitter.remove();
    foregroundEmitter.remove();
    backgroundEmitter.remove();
  };
};
