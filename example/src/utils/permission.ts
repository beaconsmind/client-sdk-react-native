import { Alert } from 'react-native';
import { RESULTS, request, Permission, check } from 'react-native-permissions';

const ALERT_TITLE = 'Permission not allowed';

export const requestPermission = (
  permission: Permission,
  onGranted: () => void,
  onDenied: () => void,
  type: string,
) => {
  request(permission).then(result => {
    switch (result) {
      case RESULTS.GRANTED:
        onGranted();
        break;
      case RESULTS.BLOCKED:
        Alert.alert(
          ALERT_TITLE,
          `${type} permission is required. Allow it in settings and restart the app.`,
        );
        onDenied();
        break;
      case RESULTS.DENIED:
        Alert.alert(
          ALERT_TITLE,
          `${type} permission is required. Allow it in settings and restart the app.`,
        );
        onDenied();
        break;
    }
  });
};

export const checkPermission = ({
  onEnabled,
  permission,
  onDisabled,
  permissionName,
  checkOnly,
}: {
  onEnabled: () => void;
  onDisabled: () => void;
  permission: Permission;
  permissionName: string;
  checkOnly?: boolean;
}) => {
  check(permission)
    .then((result: any) => {
      switch (result) {
        case RESULTS.DENIED:
          onDisabled();
          if (checkOnly) {
            return;
          }
          requestPermission(permission, onEnabled, onDisabled, permissionName);
          break;
        case RESULTS.GRANTED:
          onEnabled();
          break;
        case RESULTS.BLOCKED:
          Alert.alert(
            ALERT_TITLE,
            `Go to settings and allow ${permissionName} permission`,
          );
          onDisabled();
          break;
      }
    })
    .catch(onDisabled);
};
