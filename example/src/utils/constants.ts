export const BACKEND_URLS = [
  'https://test-develop-suite.azurewebsites.net',
  'https://test-release-suite.azurewebsites.net',
];

export const HOSTNAME_MAPPINGS = {
  'https://test-develop-suite.azurewebsites.net': 'debug',
  'https://test-release-suite.azurewebsites.net': 'release',
};
