import ImportForm from '@features/auth/ImportForm';
import React from 'react';
import { View, StyleSheet } from 'react-native';

const Import = () => {
  return (
    <View style={styles.container}>
      <ImportForm />
    </View>
  );
};

export default Import;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    gap: 16,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: '100%',
  },
});
