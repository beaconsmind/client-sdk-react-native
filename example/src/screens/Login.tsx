import LoginForm from '@features/auth/LoginForm';
import React from 'react';
import { View, StyleSheet } from 'react-native';

const Login = () => {
  return (
    <View style={styles.container}>
      <LoginForm />
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    gap: 16,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: '100%',
  },
});
