import ResetPasswordFormForm from '@features/profile/ResetPasswordForm';
import React from 'react';
import { View, StyleSheet } from 'react-native';

const ResetPasswordForm = () => {
  return (
    <View style={styles.container}>
      <ResetPasswordFormForm />
    </View>
  );
};

export default ResetPasswordForm;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    gap: 16,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: '100%',
  },
});
