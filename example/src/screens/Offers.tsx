import useNotificationPermissions from '@hooks/useNotificationsPermission';
import React, { useCallback, useState } from 'react';
import { StyleSheet, Text, FlatList, View } from 'react-native';
import * as Beaconsmind from '@beaconsmind/react-native-sdk';
import Offer from '@features/offers/Offer';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { StackNavigation } from '@navigation/main/OffersNavigator';

const keyExtractor = (offer: Beaconsmind.Offer) => offer.offerId.toString();

const Offers = () => {
  const [offers, setOffers] = useState<Beaconsmind.Offer[]>();

  useNotificationPermissions();

  const fetchOffers = useCallback(() => {
    Beaconsmind.loadOffers().then(setOffers);
  }, []);

  useFocusEffect(fetchOffers);

  const navigation = useNavigation<StackNavigation>();

  return (
    <View style={styles.container}>
      {offers && (
        <FlatList
          keyExtractor={keyExtractor}
          style={styles.list}
          data={offers}
          renderItem={({ item }) => (
            <Offer
              item={item}
              onPress={() => navigation.navigate('Offer', item)}
            />
          )}
          extraData={offers}
          ListEmptyComponent={<Text style={styles.empty}>No offers.</Text>}
        />
      )}
    </View>
  );
};

export default Offers;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    gap: 16,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: '100%',
  },
  empty: {
    marginTop: 10,
  },
  list: {
    paddingTop: 20,
    width: '100%',
    paddingHorizontal: 10,
  },
});
