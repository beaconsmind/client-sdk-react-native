import React, { useCallback, useEffect, useState } from 'react';
import { SafeAreaView, StyleSheet, FlatList, View, Text } from 'react-native';
import * as Beaconsmind from '@beaconsmind/react-native-sdk';
import useBluetoothPermission from '@hooks/useBluetoothPermission';
import useLocationPermission from '@hooks/useLocationPermission';
import Beacon from '@features/beacons/Beacon';
import { handleNotifications } from '@utils/notifications';
import { useNavigation } from '@react-navigation/native';
import { StackNavigation } from '@navigation/main/BeaconsNavigator';

const keyExtractor = (item: Beaconsmind.BeaconContactSummary) =>
  item.uuid + item.name;

const REFRESH_LIST_INTERVAL = 1000;

const Beacons = () => {
  const { enabled: bluetoothEnabled } = useBluetoothPermission();
  const { enabled: locationEnabled } = useLocationPermission();
  const [beacons, setBeacons] = useState<Beaconsmind.BeaconContactSummary[]>();

  const { navigate } = useNavigation<StackNavigation>();

  const navigateToOffer = useCallback(
    async (offerId: number) => {
      const offer = await Beaconsmind.loadOffer(offerId);
      navigate('Offer', offer);
    },
    [navigate],
  );

  useEffect(() => {
    const remove = handleNotifications({
      onOpen: navigateToOffer,
    });

    return remove;
  }, [navigateToOffer]);

  useEffect(() => {
    let interval = null;
    if (locationEnabled && bluetoothEnabled) {
      (async () => {
        await Beaconsmind.startListeningBeacons();
        interval = setInterval(async () => {
          const summary = await Beaconsmind.getBeaconContactsSummary();
          setBeacons(
            summary.sort(
              (
                item1: Beaconsmind.BeaconContactSummary,
                item2: Beaconsmind.BeaconContactSummary,
              ) => Number(item2.isInRange) - Number(item1.isInRange),
            ),
          );
        }, REFRESH_LIST_INTERVAL);
      })();

      if (interval) {
        return clearInterval(interval);
      }
    }
  }, [locationEnabled, bluetoothEnabled]);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.wrapper}>
        {bluetoothEnabled === false && (
          <Text>Bluetooth permission is required</Text>
        )}
        {locationEnabled === false && (
          <Text>Location permission is required</Text>
        )}
        {locationEnabled === undefined ||
          (bluetoothEnabled === undefined && <Text>Loading...</Text>)}
        {locationEnabled && bluetoothEnabled && beacons && (
          <FlatList
            keyExtractor={keyExtractor}
            style={styles.list}
            data={beacons}
            renderItem={Beacon}
            extraData={beacons}
            ListEmptyComponent={
              <Text style={styles.empty}>
                No beacons detected. Make sure location & bluetooth are active.
              </Text>
            }
          />
        )}
      </View>
    </SafeAreaView>
  );
};

export default Beacons;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    gap: 16,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: '100%',
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  list: {
    width: '100%',
  },
  wrapper: { paddingHorizontal: 20 },
  empty: {
    marginTop: 50,
  },
});
