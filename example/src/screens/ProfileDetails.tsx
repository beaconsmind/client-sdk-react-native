import ProfileDetailsForm from '@features/profile/ProfileDetailsForm';
import React from 'react';
import { View, StyleSheet } from 'react-native';

const ProfileDetails = () => {
  return (
    <View style={styles.container}>
      <ProfileDetailsForm />
    </View>
  );
};

export default ProfileDetails;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    gap: 16,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: '100%',
  },
});
