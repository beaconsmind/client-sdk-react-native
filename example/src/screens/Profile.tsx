import Button from '@components/Button';
import { useBeaconsmindContext } from '@features/beaconsmind/Context';
import AppInfo from '@features/shared/AppInfo';
import { StackNavigation } from '@navigation/main/ProfileNavigator';
import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

const Profile = () => {
  const navigation = useNavigation<StackNavigation>();
  const { logout, user } = useBeaconsmindContext();

  const welcomeText = user?.imported
    ? 'Profile functionality disabled for imported accounts'
    : `Welcome, ${user?.firstName} ${user?.lastName}`;

  const dynamicStylesheet = StyleSheet.create({
    welcome: {
      fontSize: 20,
      color: user?.imported ? '#f06363' : undefined,
      fontWeight: user?.imported ? 'bold' : undefined,
    },
  });

  return (
    <View style={styles.wrapper}>
      <Text style={dynamicStylesheet.welcome}>{welcomeText}</Text>
      <View style={styles.container}>
        <Button
          disabled={user?.imported}
          text="PROFILE DETAILS"
          onPress={() => navigation.navigate('Profile Details')}
        />
        <Button
          disabled={user?.imported}
          text="RESET PASSWORD"
          onPress={() => navigation.navigate('Reset Password')}
        />
        <Button text="LOGOUT" onPress={logout} />
      </View>
      <AppInfo />
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    gap: 16,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: '100%',
    paddingHorizontal: 40,
  },
  wrapper: {
    flex: 1,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 20,
  },
});
