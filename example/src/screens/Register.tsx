import RegisterForm from '@features/auth/RegisterForm';
import React from 'react';
import { StyleSheet, View } from 'react-native';

const Register = () => {
  return (
    <View style={styles.container}>
      <RegisterForm />
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    gap: 16,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: '100%',
  },
});
