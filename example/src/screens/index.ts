import Login from './Login';
import Register from './Register';
import Offers from './Offers';
import Profile from './Profile';
import Beacons from './Beacons';
import ResetPassword from './ResetPassword';
import ProfileDetails from './ProfileDetails';
import Offer from './Offer';
import Import from './Import';

export {
  Login,
  Import,
  Register,
  Offers,
  Profile,
  Beacons,
  ProfileDetails,
  ResetPassword,
  Offer,
};
