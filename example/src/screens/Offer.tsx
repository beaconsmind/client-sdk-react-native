import React, { useEffect } from 'react';
import {
  Image,
  Linking,
  ScrollView,
  StyleSheet,
  Text,
  View,
  useWindowDimensions,
} from 'react-native';
import * as Beaconsmind from '@beaconsmind/react-native-sdk';
import Button from '@components/Button';
import RenderHtml from 'react-native-render-html';

const Offer = ({ route }: any) => {
  const offer: Beaconsmind.Offer = route.params;

  useEffect(() => {
    Beaconsmind.markOfferAsRead(route.params.offerId);
  }, [route.params]);

  const { width } = useWindowDimensions();

  return (
    <View style={styles.wrapper}>
      <ScrollView contentContainerStyle={styles.container}>
        <Image source={{ uri: offer.imageUrl }} style={styles.image} />
        <Text style={styles.title}>{offer.title}</Text>
        <View style={styles.htmlContainer}>
          <RenderHtml source={{ html: offer.text }} contentWidth={width} />
        </View>
        {!!offer.callToAction && (
          <View style={styles.button}>
            <Button
              text={offer.callToAction.title || 'Open'}
              onPress={() => {
                Beaconsmind.markOfferAsClicked(offer.offerId);
                Linking.openURL(offer.callToAction?.link || '');
              }}
            />
          </View>
        )}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  container: {
    gap: 20,
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 30,
  },
  title: { fontSize: 22 },
  image: { width: '90%', height: 250, resizeMode: 'contain' },
  button: {
    width: '90%',
  },
  htmlContainer: {
    paddingHorizontal: 20,
  },
});

export default Offer;
