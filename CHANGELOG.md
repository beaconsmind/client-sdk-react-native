

## [1.4.3](https://gitlab.com/beaconsmind/client-sdk-react-native/compare/v1.4.2...v1.4.3) (2024-02-14)


### Bug Fixes

* build issues ([6efa9ab](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/6efa9ab6710f2e634eeba7cf9b4f7be6ec404f82))

## [1.4.2](https://gitlab.com/beaconsmind/client-sdk-react-native/compare/v1.4.1...v1.4.2) (2024-02-08)

### Bug Fixes

- fix potential issue with importAccount functionality ([ebd6723](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/ebd6723fd66c569a6c8733e662bf8e229be6e236))
- set log level to debug on first run ([a8d465d](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/a8d465d7df7b1bf2b7ad515943dc3d6c78568e22))

## [1.4.1](https://gitlab.com/beaconsmind/client-sdk-react-native/compare/v1.4.0...v1.4.1) (2023-10-10)

### Bug Fixes

- add nullcheck on current activity ([dff1e88](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/dff1e8860d210e44aa1609d60b7f90650232eb04))
- adjust permission alerts ([40bb6d6](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/40bb6d62b8542eed73f18b2b08e1e0751491f075))
- fn name ([9de4e36](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/9de4e36d7c9ac0a8dc319bd2f16df1af3f8c134b))
- ios build ([ace6c0f](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/ace6c0f2c545aa9a3ce4b744e280fa76854a5439))
- offer screen ([6b61607](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/6b6160738170ae3b4f0ad07a744589cb3e632890))
- offer screen adjustments ([5ca2c0b](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/5ca2c0b07c7d99e4dde3b35acabd08d5416c9e06))
- offers list ui adjustments ([8562879](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/85628790e8f250bab59fa2af5addbc2b2207edfd))

### Features

- beacons tab ([41fb06d](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/41fb06dc561b3578448c9fa03ba3c0799272a1cf))
- handle open from background ([662e021](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/662e0215dc59593bc235dd92c0583ea59bf89b62))
- implement app authentication ([c5aaed4](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/c5aaed47a1762aeffb6c2ed35b9ca28e36c84072))
- offers tab ([361df33](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/361df33cd8b5ca2ad62e912431e374a5770214e5))
- profile tab features ([9679454](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/9679454a468194e61a833600664cae73e9dab101))
- tab navigation ([de093f1](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/de093f17029990b259951a2482689605be8f2017))

# [1.4.0](https://gitlab.com/beaconsmind/client-sdk-react-native/compare/v1.3.0...v1.4.0) (2023-05-30)

### Features

- add countryCode parameter to the signUp method ([e558fab](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/e558fab369bbe55aed98b7c147d1189405a86ad4))
- upgrade ios sdk to 3.8.3 ([20631e2](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/20631e2f82f8f7c99986e8351aba61a5e1f109b5))

# [1.3.0](https://gitlab.com/beaconsmind/client-sdk-react-native/compare/v1.2.0...v1.3.0) (2023-05-16)

### Bug Fixes

- change 'example' to 'React SDK Demo' for consistency ([c3b9e80](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/c3b9e80f92aa18f58bc801b0f0b9b622753d3c46))
- mark offer as received when received in background ([5a6dacd](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/5a6dacdb54254aecebdae3a5916d46e2787f0417))

### Features

- **example:** support multiple messaging services ([e2c5c2c](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/e2c5c2cc8a2c4f47a894c30b77c0962aabef8794))

# [1.2.0](https://gitlab.com/beaconsmind/client-sdk-react-native/compare/v1.1.1...v1.2.0) (2023-04-14)

### Bug Fixes

- **example:** change iOS example app launch style to automatic ([24a8aad](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/24a8aad00300d67b056672fd218100cb73de1ff7))

### Features

- upgrade android sdk to 1.12.0 ([fabbe85](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/fabbe85053ceeb568e23b19a0ae2ba42529beaad))

## [1.1.1](https://gitlab.com/beaconsmind/client-sdk-react-native/compare/v1.1.0...v1.1.1) (2023-03-22)

### Bug Fixes

- add POST_NOTIFICATIONS permission ([1fba271](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/1fba271e3d9da8b0d2a9eefde84f05d423e82c03))
- **example:** call registerRemoteNotifications to trigger FCM token update ([0125867](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/0125867339c1c30f2774f52da5062ff8622715a9))
- **example:** tweak notification handling ([d6fae9e](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/d6fae9ee26f0ace46c4da4bfb9431f511eb1297b))

# [1.1.0](https://gitlab.com/beaconsmind/client-sdk-react-native/compare/v1.0.2...v1.1.0) (2023-03-22)

### Bug Fixes

- android notification option field names ([0b093cb](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/0b093cb627a5f6e6e6ede6f6f9e92c69906eac11))
- **example:** minor tweaks to the example app ([72c7535](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/72c7535c4afd6321de673a313b479f1ad0349472))
- export TS models ([a43c677](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/a43c677ddd39dc4bf330ce0611671481d6d86b16))
- implement markOfferAsClicked ([737d4c4](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/737d4c4a3d72d260cd198d6777bae46f65034a4f))
- rename buttonModel to callToAction in Android SDK ([c666351](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/c666351aa0481fd3506a3f5a55cfce34ffbc65bf))

### Features

- **example:** use react-native-notifications package to handle push notifications ([808b3c6](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/808b3c6e959dc00e9d4aab8b9cf97215aba89de2))

## [1.0.2](https://gitlab.com/beaconsmind/client-sdk-react-native/compare/v1.0.1...v1.0.2) (2023-03-13)

### Bug Fixes

- unsubscribe for push notifications on logout ([31e9159](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/31e9159fc380839ccfbc6d13c0264cb25998cc97))

## [1.0.1](https://gitlab.com/beaconsmind/client-sdk-react-native/compare/v1.0.0...v1.0.1) (2023-02-23)

### Bug Fixes

- markOfferAsReceived on iOS ([0a69cbb](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/0a69cbbf6f9f395fd111cd21e0bb686a9f56696d))
- markOfferAsRedemeed on iOS ([3bce320](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/3bce320b5f7d87d4a2d0be89234ebfd69a99100b))
- rename Offer.buttonModel to Offer.callToAction ([90bb90d](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/90bb90d09197b7ba7559104a295c79b4c2aa2799))

## [1.0.0](https://gitlab.com/beaconsmind/client-sdk-react-native/compare/v1.0.0-rc.4...v1.0.0) (2023-02-20)

### Bug Fixes

- add BeaconsmindSdkInitializer to the package manifest ([2a11506](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/2a1150640215e0a8b751f7df6c0f805b7faf0a39))
- **example:** set entry file so that release build works ([3347c85](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/3347c85da99a958d27d90ba998304d6c5526afee))
- **example:** tweak Podfile so that app can be built on Xcode 14 ([863f5fb](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/863f5fb4eb78c35335fb3b8a374f3f31753812ce))
- set shouldStartService to false in importAccount ([897b53a](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/897b53a84a3978b1deaf3bca7758b3d5307103cf))
- tweak initialise options type ([667ebea](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/667ebea70ec5c7a357f4897074ca52855312c3e1))
- use startListeningBeacons(null) to replicate old behaviour ([5fe0129](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/5fe0129528ca53c0dd665b0c0afeea5e0b0d2574))

### Features

- **example:** handle push notifications using firebase messaging ([2aa126b](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/2aa126b5c3061925f56945eba710fe8f91f9ba91))
- **example:** request permissions ([a2e2d02](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/a2e2d0206433a673a32f26e0d7beb28731fe55b9))
- implement initializeDevelop method in TypeScript ([4f29739](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/4f29739b13f6576e5df5df93a2acbf5565bb01f4))
- implement initializeDevelop method on Android & do not pass appVersion ([88d8d71](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/88d8d71fbfb8eb864078537cbe64ba5e8ebacbbe))
- implement initializeDevelop method on iOS ([802ff70](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/802ff707c3a422bf79bf9ebf24a6ca5582025525))
- implement markOfferAsClicked method on Android ([2c02c55](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/2c02c5559711717ab24a1f95bcd1310f46657faf))
- update android sdk from 1.10.0 to 1.11.2 ([d8da512](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/d8da512b449eb10a5cc41d23aa945f0938f01fc4))
- update iOS sdk from 3.6.4 to 3.7.2 ([8bbfa30](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/8bbfa3058df7cfadf60d54bf96278166e21bb256))

## [1.0.0-rc.4](https://gitlab.com/beaconsmind/client-sdk-react-native/compare/v1.0.0-rc.3...v1.0.0-rc.4)) (2022-08-22)

### Bug Fixes

- **example:** building example iOS app in Xcode ([37ab841](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/37ab841e1344b4bb2ffc62598ff20b45ca3ce97a))

### Features

- add setMinLogLevel ([4d31c90](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/4d31c90c9f557ae042f1525437ce3b8b9396bff1))
- prepare initial release ([9d3b987](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/9d3b98768f729e9415dd723eb7d3ffcfb1ad9d65))
- upgrade iOS SDK from 3.4.2 to 3.6.1 ([215e679](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/215e67974f4eadba7b8fad837f1f540c6341a221))

## [1.0.0-rc.3](https://gitlab.com/beaconsmind/client-sdk-react-native/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2022-05-11)

### Bug Fixes

- repair confirmPassword argument for signup ([3c7c99e](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/3c7c99ece502f36c6cfa145fa28e654c8cdfb53b))

## [1.0.0-rc.2](https://gitlab.com/beaconsmind/client-sdk-react-native/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2022-05-11)

### Bug Fixes

- add missing errors to promise rejections ([ee4a78b](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/ee4a78ba54306c7a8967b46106cdaa43984676f3))
- call signup method instead of signUp ([58700c9](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/58700c9cb186ff07d87cf63764a13c63ef123d38))

## 1.0.0-rc.1 (2022-05-11)

### Features

- prepare initial release ([9d3b987](https://gitlab.com/beaconsmind/client-sdk-react-native/commit/9d3b98768f729e9415dd723eb7d3ffcfb1ad9d65))