package com.beaconsmindreactnativesdk

import com.beaconsmind.api.models.*
import com.beaconsmind.sdk.*
import com.beaconsmind.api.models.ChangePasswordRequest;
import com.beaconsmind.sdk.models.LogLevel
import com.facebook.react.bridge.*
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter
import java.sql.Date
import java.sql.Timestamp
import java.text.DateFormat
import java.text.SimpleDateFormat


class BeaconsmindReactNativeSDK(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {
  override fun getName(): String {
    return "BeaconsmindReactNativeSDK"
  }

  @ReactMethod
  fun initialize(options: ReadableMap, promise: Promise) {
    currentActivity?.runOnUiThread {
      try {
        if (!options.hasKey("hostname")) {
          throw Exception("Hostname is required.")
        }

        val application = currentActivity?.application
        if (application == null) {
          throw Exception("Application context not found.")
        }

        val hostname = options.getString("hostname")!!
        val usePassiveScanning = options.getBoolean("usePassiveScanning", true)!!
        val notificationTitle = options.getString("notificationTitle", "Beaconsmind")!!
        val notificationText = options.getString("notificationText", "Listening for Beacons")!!
        val notificationChannelName = options.getString("notificationChannelName", "beaconsmind")!!

        val notificationBadgeName = options.getString("notificationBadgeName", "ic_beacons")
        val notificationBadge = application.resources.getIdentifier(
          notificationBadgeName,
          "drawable",
          application.packageName
        )

        val config = BeaconsmindConfig.Builder(hostname)
          .usePassiveScanning(usePassiveScanning)
          .setNotificationBadge(notificationBadge)
          .setNotificationTitle(notificationTitle)
          .setNotificationText(notificationText)
          .setNotificationChannelName(notificationChannelName)
          .build(application)

        BeaconsmindSdk.initialize(application, config)

        promise.resolve(true)
      } catch (e: Exception) {
        promise.reject("500", "initialize failed: $e")
      }
    }
  }

  @ReactMethod
  fun initializeDevelop(options: ReadableMap, promise: Promise) {
    currentActivity?.runOnUiThread {
      try {
        if (!options.hasKey("hostname")) {
          throw Exception("Hostname is required.")
        }

        val application = currentActivity?.application
        if (application == null) {
          throw Exception("Application context not found.")
        }

        val hostname = options.getString("hostname")!!
        val usePassiveScanning = options.getBoolean("usePassiveScanning", true)!!
        val notificationTitle = options.getString("notificationTitle", "Beaconsmind")!!
        val notificationText = options.getString("notificationText", "Listening for Beacons")!!
        val notificationChannelName = options.getString("notificationChannelName", "beaconsmind")!!

        val notificationBadgeName = options.getString("notificationBadgeName", "ic_beacons")
        val notificationBadge = application.resources.getIdentifier(
          notificationBadgeName,
          "drawable",
          application.packageName
        )

        val config = BeaconsmindConfig.Builder(hostname)
          .usePassiveScanning(usePassiveScanning)
          .setNotificationBadge(notificationBadge)
          .setNotificationTitle(notificationTitle)
          .setNotificationText(notificationText)
          .setNotificationChannelName(notificationChannelName)
          .build(application)

        BeaconsmindSdk.initializeDevelop(currentActivity!!, config)

        promise.resolve(true)
      } catch (e: Exception) {
        promise.reject("500", "initialize failed: $e")
      }
    }
  }

  /// This code is deprecated. Use initialize instead.
  @ReactMethod
  fun start(options: ReadableMap, promise: Promise) {
    try {
      val appVersion = options.getString("appVersion")
      val hostname = options.getString("hostname")
      val notificationBadgeName = options.getString("androidNotificationBadgeName")
      val notificationTitle = options.getString("androidNotificationTitle")
      val notificationText = options.getString("androidNotificationText")
      val notificationChannelName = options.getString("androidNotificationChannelName")

      var notificationBadge: Int? = null
      if (notificationBadgeName != null && reactApplicationContext != null) {
        notificationBadge =
          reactApplicationContext!!.applicationContext.resources.getIdentifier(
            notificationBadgeName,
            "drawable",
            reactApplicationContext!!.applicationContext.packageName
          )
      }

      val config = BeaconsmindConfig.Builder(hostname!!)
        .setAppVersion(appVersion!!)
        .setNotificationBadge(notificationBadge ?: R.drawable.ic_beacons)
        .setNotificationTitle(notificationTitle ?: "Beaconsmind")
        .setNotificationText(notificationText ?: "Listening for Beacons")
        .setNotificationChannelName(
          notificationChannelName ?: "beaconsmind"
        )
        .build()

      currentActivity!!.runOnUiThread {
        BeaconsmindSdk.initialize(currentActivity?.application!!, config)
        promise.resolve(true)
      }
    } catch (e: Exception) {
      promise.reject("500", e.toString())
    }
  }

  @ReactMethod
  fun signup(options: ReadableMap, promise: Promise) {
    try {
      val username = options.getString("username")!!
      val firstName = options.getString("firstName")!!
      val lastName = options.getString("lastName")!!
      val password = options.getString("password")!!
      val confirmPassword = options.getString("confirmPassword")!!
      val language = options.getString("language", null)
      val gender = options.getString("gender", null)
      val favoriteStoreID = options.getInt("favoriteStoreID", null)
      val birthDateSeconds = options.getDouble("birthDateSeconds", null)
      val countryCode = options.getString("countryCode", null)

      var birthday: Date? = null
      if (birthDateSeconds != null) {
        val stamp = Timestamp(birthDateSeconds.toLong() * 1000)
        birthday = Date(stamp.time)
      }

      val request = CreateUserRequest()
      request.username = username
      request.firstName = firstName
      request.lastName = lastName
      request.password = password
      request.confirmPassword = confirmPassword
      request.language = language
      request.gender = gender
      request.favoriteStoreId = favoriteStoreID
      request.birthday = birthday
      request.countryCode = countryCode

      UserManager.getInstance().createAccount(request, object : OnCompleteListener {
        override fun onSuccess() {
          val userId = UserManager.getUserIdOrNull()
          if (userId != null) {
            val result = Arguments.createMap()
            result.putString("userId", userId)
            promise.resolve(result)
          } else {
            promise.resolve(null)
          }
        }

        override fun onError(e: Error?) {
          promise.reject("500", "signup failed: $e")
        }
      }
      )
    } catch (e: Exception) {
      promise.reject("500", "signup failed: $e")
    }
  }

  @ReactMethod
  fun login(options: ReadableMap, promise: Promise) {
    try {
      val userName = options.getString("username")!!
      val password = options.getString("password")!!

      // TODO: investigate error: kotlin.UninitializedPropertyAccessException: lateinit property INSTANCE has not been initialized
      UserManager.getInstance()
        .login(userName, password, object : OnCompleteListener {

          override fun onSuccess() {
            val userId = UserManager.getUserIdOrNull()
            if (userId != null) {
              val result = Arguments.createMap()
              result.putString("userId", userId)
              promise.resolve(result)
            } else {
              promise.resolve(null)
            }
          }

          override fun onError(error: Error?) {
            promise.reject("500", "login failed: $error")
          }
        }
        )
    } catch (e: Exception) {
      promise.reject("500", "login failed: $e")
    }
  }

  @ReactMethod
  fun importAccount(options: ReadableMap, promise: Promise) {
    try {
      val id = options.getString("id")!!
      val email = options.getString("email")!!
      val firstName = options.getString("firstName")
      val lastName = options.getString("lastName")
      val language = options.getString("language")
      val gender = options.getString("gender")
      val birthDateSeconds = options.getDouble("birthDateSeconds", null)

      var birthday: Date? = null
      if (birthDateSeconds != null) {
        val stamp = Timestamp(birthDateSeconds.toLong() * 1000)
        birthday = Date(stamp.time)
      }

      val request = ImportUserRequest()
      request.id = id
      request.email = email
      request.firstName = firstName
      request.lastName = lastName
      request.language = language
      request.gender = gender
      request.birthDate = birthday

      UserManager.getInstance().importAccount(request, object : OnCompleteListener {

        override fun onSuccess() {
          val userId = UserManager.getUserIdOrNull()
          if (userId != null) {
            val result = Arguments.createMap()
            result.putString("userId", userId)
            promise.resolve(result)
          } else {
            promise.resolve(null)
          }
        }

        override fun onError(e: Error) {
          promise.reject("500", "importAccount failed: $e")
        }
      }
      )
    } catch (e: Exception) {
      promise.reject("500", "importAccount failed: $e")
    }
  }

  @ReactMethod
  fun logout(promise: Promise) {
    try {
      UserManager.getInstance().logout(object : OnCompleteListener {
        override fun onSuccess() {
          promise.resolve(true)
        }
        override fun onError(error: Error?) {
          promise.reject("500", "logout failed: $error")
        }
      })
    } catch (e: Exception) {
      promise.reject("500", "logout failed: $e")
    }
  }

  @ReactMethod
  fun getProfile(promise: Promise) {
    try {
      UserManager
        .getInstance()
        .getAccount(object : OnResultListener<ProfileResponse> {
          override fun onError(e: Error) {
            promise.reject("500", "getProfile failed $e")
          }
          override fun onSuccess(
            profileResponse: ProfileResponse?
          ) {
            promise.resolve(profileResponse?.toWritableMap())
          }
        })
    } catch (e: Exception) {
      promise.reject("500", "getProfile failed: $e")
    }
  }

  @ReactMethod
  fun updateProfile(options: ReadableMap, promise: Promise) {
    try {
      val firstName = options.getString("firstName")!!
      val lastName = options.getString("lastName")!!
      val city = options.getString("city")
      val country = options.getString("country")
      val disablePushNotifications = options.getBoolean("disablePushNotifications", null)
      val favoriteStoreID = options.getInt("favoriteStoreID", null)
      val gender = options.getString("gender")
      val houseNumber = options.getString("houseNumber")
      val landlinePhone = options.getString("landlinePhone")
      val language = options.getString("language")
      val phoneNumber = options.getString("phoneNumber")
      val street = options.getString("street")
      val zipCode = options.getString("zipCode")
      val birthDateSeconds = options.getDouble("birthDateSeconds", null)

      var birthday: Date? = null
      if (birthDateSeconds != null) {
        val stamp = Timestamp(birthDateSeconds.toLong() * 1000)
        birthday = Date(stamp.time)
      }

      val request = UpdateProfileRequest()
      request.firstName = firstName
      request.lastName = lastName
      request.birthDate = birthday
      request.city = city
      request.country = country
      request.disablePushNotifications = disablePushNotifications?.toString()
      request.favoriteStoreId = favoriteStoreID
      request.gender = gender
      request.houseNumber = houseNumber
      request.landlinePhone = landlinePhone
      request.language = language
      request.phoneNumber = phoneNumber
      request.street = street
      request.zipCode = zipCode

      UserManager.getInstance().updateAccount(request, object : OnCompleteListener {
        override fun onSuccess() {
          UserManager.getInstance()
            .getAccount(object : OnResultListener<ProfileResponse> {
              override fun onError(e: Error) {
                promise.reject("500", "updateProfile failed: $e")
              }

              override fun onSuccess(profileResponse: ProfileResponse?) {
                promise.resolve(profileResponse?.toWritableMap())
              }
            })
        }

        override fun onError(e: Error) {
          promise.reject("500", "updateProfile failed: $e")
        }
      })
    } catch (e: Exception) {
      promise.reject("500", "updateProfile failed: $e")
    }
  }

  @ReactMethod
  fun changePassword(options: ReadableMap, promise: Promise) {
    try {
      val oldPassword = options.getString("oldPassword")!!
      val newPassword = options.getString("newPassword")!!
      val request = ChangePasswordRequest()
      request.oldPassword = oldPassword
      request.newPassword = newPassword

      UserManager.getInstance().changePassword(request, object : OnCompleteListener {
        override fun onSuccess() {
          UserManager.getInstance()
            .getAccount(object : OnResultListener<ProfileResponse> {
              override fun onError(e: Error) {
                promise.reject("500", "changePassword failed: $e")
              }

              override fun onSuccess(profileResponse: ProfileResponse?) {
                promise.resolve(profileResponse?.toWritableMap())
              }
            })
        }

        override fun onError(e: Error) {
          promise.reject("500", "changePassword failed: $e")
        }
      })
    } catch (e: Exception) {
      promise.reject("500", "changePassword failed: $e")
    }
  }

  @ReactMethod
  fun getOAuthContext(promise: Promise) {
    try {
      var userId = UserManager.getUserIdOrNull()
      if (userId != null) {
        var map = Arguments.createMap()
        map.putString("userId", userId)
        promise.resolve(map)
      } else {
        promise.resolve(null)
      }
    } catch (e: Exception) {
      promise.reject("500", "getOAuthContext failed: $e")
    }
  }

  @ReactMethod
  fun updateHostname(options: ReadableMap, promise: Promise) {
    try {
      if (!options.hasKey("hostname")) {
        throw Exception("Hostname is required.")
      }

      val hostname = options.getString("hostname")!!
      BeaconsmindSdk.updateSuiteUri(hostname)
    } catch (e: Exception) {
      promise.reject("500", "updateHostname failed: $e")
    }
  }

  @ReactMethod
  fun startListeningBeacons(promise: Promise) {
    try {
      BeaconListenerService.getInstance().startListeningBeacons(null)
      promise.resolve(true)
    } catch (e: Exception) {
      promise.reject("500", "startListeningBeacons failed: $e")
    }
  }

  @ReactMethod
  fun stopListeningBeacons(promise: Promise) {
    try {
      BeaconListenerService.getInstance().stopListeningBeacons()
      promise.resolve(true)
    } catch (e: Exception) {
      promise.reject("500", "stopListeningBeacons failed: $e")
    }
  }

  /// This code is deprecated. Use getBeaconContactsSummary instead.
  @ReactMethod
  fun getBeaconsSummary(promise: Promise) {
    val beacons = BeaconListenerService.getInstance().beaconsSummary
    val result = Arguments.createArray()
    beacons.forEach {
      val entry = Arguments.createMap()
      entry.putString("uuid", it.uuid)
      entry.putString("major", it.major)
      entry.putString("minor", it.minor)
      entry.putString("name", it.name)
      entry.putString("store", it.store)
      result.pushMap(entry)
    }
    promise.resolve(result)
  }

  @ReactMethod
  fun getBeaconContactsSummary(promise: Promise) {
    try {
      val beacons = BeaconListenerService.getInstance().beaconsSummary

      val result = Arguments.createArray()
      beacons.forEach {
        val entry = Arguments.createMap()
        entry.putString("uuid", it.uuid)
        entry.putString("major", it.major)
        entry.putString("minor", it.minor)
        entry.putString("name", it.name)
        entry.putString("store", it.store)
        entry.putDouble("currentRSSI", it.currentRssi)
        entry.putDouble("averageRSSI", it.averageRssi)
        /// WritableMap doesn't support Long types, so we have to convert timestamp to Double.
        entry.putDouble("lastContactTimestamp", it.getLastContact()?.toDouble())
        entry.putInt("contactFrequencyPerMinute", it.getFrequency())
        entry.putInt("contactsCount", it.timesContacted)
        entry.putBoolean("isInRange", it.isInRange)

        result.pushMap(entry)
      }

      promise.resolve(result)
    } catch (e: Exception) {
      promise.reject("500", "getBeaconContactsSummary failed: $e")
    }
  }

  @ReactMethod
  fun registerDeviceToken(options: ReadableMap, promise: Promise) {
    try {
      val token = options.getString("deviceToken")!!
      UserManager.getInstance().updateDeviceInfo(token)
      promise.resolve(true)
    } catch (e: Exception) {
      promise.reject("500", "registerDeviceToken failed: $e")
    }
  }

  @ReactMethod
  fun markOfferAsRead(options: ReadableMap, promise: Promise) {
    try {
      val offerId = options.getInt("offerId")
      OffersManager.getInstance().markOfferAsRead(offerId)
      promise.resolve(true)
    } catch (e: Exception) {
      promise.reject("500", "markOfferAsRead failed: $e")
    }
  }

  @ReactMethod
  fun markOfferAsClicked(options: ReadableMap, promise: Promise) {
    try {
      val offerId = options.getInt("offerId")
      OffersManager.getInstance().markOfferClicked(offerId)
      promise.resolve(true)
    } catch (e: Exception) {
      promise.reject("500", "markOfferAsClicked failed: $e")
    }
  }

  @ReactMethod
  fun markOfferAsReceived(options: ReadableMap, promise: Promise) {
    try {
      val offerId = options.getInt("offerId")
      OffersManager.getInstance().markOfferAsReceived(offerId)
      promise.resolve(true)
    } catch (e: Exception) {
      promise.reject("500", "markOfferAsReceived failed: $e")
    }
  }

  @ReactMethod
  fun markOfferAsRedeemed(options: ReadableMap, promise: Promise) {
    try {
      val offerId = options.getInt("offerId")
      OffersManager.getInstance()
        .markOfferRedeemed(offerId, object : OnCompleteListener {
          override fun onSuccess() {
            promise.resolve(true)
          }

          override fun onError(error: Error) {
            promise.reject("500", "markOfferAsRedeemed failed: $error")
          }
        })
    } catch (e: Exception) {
      promise.reject("500", "markOfferAsRedeemed failed: $e")
    }
  }

  @ReactMethod
  fun loadOffers(promise: Promise) {
    try {
      OffersManager.getInstance()
        .loadOffers(object : OnResultListener<List<OfferResponse>> {
          override fun onError(error: Error) {
            promise.reject("500", "loadOffers failed: $error")
          }

          override fun onSuccess(response: List<OfferResponse>) {
            val result = Arguments.createArray()
            response.forEach { offer -> result.pushMap(offer.toWriteableMap()) }
            promise.resolve(result)
          }
        })
    } catch (e: Exception) {
      promise.reject("500", "loadOffers failed: $e")
    }
  }

  @ReactMethod
  fun loadOffer(options: ReadableMap, promise: Promise) {
    try {
      val offerId = options.getInt("offerId")
      OffersManager.getInstance()
        .loadOffer(offerId, object : OnResultListener<OfferResponse> {
          override fun onError(error: Error) {
            promise.reject("500", "loadOffer failed: $error")
          }

          override fun onSuccess(
            response: OfferResponse
          ) {
            promise.resolve(response.toWriteableMap())
          }
        })
    } catch (e: Exception) {
      promise.reject("500", "loadOffer failed: $e")
    }
  }

  @ReactMethod
  fun setMinLogLevel(options: ReadableMap, promise: Promise) {
    try {
      val minLogLevelString = options.getString("minLogLevel", "")!!
      val minLogLevel = LogLevel.fromValue(minLogLevelString)

      BeaconsmindSdk.minLogLevel = minLogLevel

      promise.resolve(true)
    } catch (e: Exception) {
      promise.reject("500", "setMinLogLevel failed: $e")
    }
  }
}

fun OfferResponse.toWriteableMap(): WritableMap {
  val entry = Arguments.createMap()
  entry.putInt("offerId", offerId)
  entry.putString("title", title)
  entry.putString("text", text)
  entry.putString("imageUrl", imageUrl)
  entry.putString("thumbnailUrl", thumbnailUrl)
  entry.putString("validity", validity)
  entry.putBoolean("isRedeemed", isRedeemed)
  entry.putBoolean("isVoucher", isVoucher)
  entry.putInt("tileAmount", tileAmount)

  callToAction?.let {
    val callToActionMap = Arguments.createMap()
    callToActionMap.putString("title", it.title)
    callToActionMap.putString("link", it.link)
    entry.putMap("callToAction", callToActionMap)
  }

  return entry
}

fun ProfileResponse.toWritableMap(): WritableMap {
  val dateFormatter: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ")

  val map = Arguments.createMap()

  map.putBoolean("disablePushNotifications", disablePushNotifications)
  map.putString("firstName", firstName)
  map.putString("joinDate", dateFormatter.format(joinDate))
  map.putString("lastName", lastName)
  map.putBoolean("newsLetterSubscription", newsLetterSubscription)
  map.putString("birthDate", birthDate?.let { dateFormatter.format(it) })
  map.putString("zipCode", zipCode)
  map.putString("street", street)
  map.putString("city", city)

  val claimsArray = Arguments.createArray()
  for (claim in claims!!) {
    claimsArray.pushString(claim)
  }
  map.putArray("claims", claimsArray)

  map.putString("clubId", clubId)
  map.putString("country", country)
  map.putString("favoriteStore", favoriteStore)
  favoriteStoreId?.let { map.putInt("favoriteStoreId", favoriteStoreId!!) }
  map.putString("fullName", fullName)
  map.putString("gender", gender)
  map.putString("houseNumber", houseNumber)
  map.putString("id", id)
  map.putString("landlinePhone", landlinePhone)
  map.putString("language", language)
  map.putString("phoneNumber", phoneNumber)

  val rolesArray = Arguments.createArray()
  for (role in roles!!) {
    rolesArray.pushString(role)
  }
  map.putArray("roles", rolesArray)

  map.putString("street", street)
  map.putString("url", url)
  map.putString("userName", userName)
  map.putString("zipCode", zipCode)

  return map
}

fun ReadableMap.getString(key: String, default: String?): String? {
  if (this.hasKey(key)) {
    return this.getString(key)
  } else {
    return default
  }
}

fun ReadableMap.getInt(key: String, default: Int?): Int? {
  if (this.hasKey(key)) {
    return this.getInt(key)
  } else {
    return default
  }
}

fun ReadableMap.getDouble(key: String, default: Double?): Double? {
  if (this.hasKey(key)) {
    return this.getDouble(key)
  } else {
    return default
  }
}

fun ReadableMap.getBoolean(key: String, default: Boolean?): Boolean? {
  if (this.hasKey(key)) {
    return this.getBoolean(key)
  } else {
    return default
  }
}

fun WritableMap.putDouble(key: String, value: Double?) {
  if (value != null) {
    this.putDouble(key, value)
  } else {
    this.putNull(key)
  }
}
