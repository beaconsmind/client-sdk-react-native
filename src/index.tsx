import { NativeModules, Platform } from 'react-native';

const LINKING_ERROR =
  "The package '@beaconsmind/react-native-sdk' doesn't seem to be linked. Make sure: \n\n" +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo managed workflow\n';

const Beaconsmind = NativeModules.BeaconsmindReactNativeSDK
  ? NativeModules.BeaconsmindReactNativeSDK
  : new Proxy(
      {},
      {
        get() {
          throw new Error(LINKING_ERROR);
        },
      }
    );

export type UserIdResponse = { userId: string } | null;

/**
 * @deprecated Use InitializeOptions instead.
 */
export interface StartOptions {
  appVersion: string;
  hostname: string;
  androidNotificationBadgeName?: string;
  androidNotificationTitle?: string;
  androidNotificationText?: string;
  androidNotificationChannelName?: string;
}

/**
 * @deprecated Use initialize instead.
 */
export function start(options: StartOptions): Promise<boolean> {
  return Beaconsmind.start(options);
}

export interface InitializeOptions {
  appVersion: string;
  hostname: string;
  platformOptions?: InitializePlatformOptions;
}

export interface InitializePlatformOptions {
  android?: InitializeAndroidOptions;
}

export interface InitializeAndroidOptions {
  /**
   * Options that describe the content of the native Android notification that
   * is displayed to user when a foreground service responsible for scanning.
   *
   * The notification is displayed when [usePassiveScanning] is set to `false`.
   */
  notification: AndroidNotificationOptions | null;

  /**
   * The SDK supports two types of BLE scanning:
   * - active scanning,
   * - passive scanning.
   *
   * When scanning actively, the Beaconsmind SDK will start a foreground service and show a pinned notification described by [notificationOptions].
   * By default, the SDK uses passive scanning, i.e. [usePassiveScanning] is set to true.
   */
  usePassiveScanning?: boolean;
}

export interface AndroidNotificationOptions {
  /**
   * Name of the native icon resource.
   *
   * Default value: "ic_beacons".
   *
   * Reference: https://developer.android.com/reference/androidx/core/app/NotificationCompat.Builder#setSmallIcon(int)
   */
  notificationBadgeName: string;

  /**
   * Title of the notification (first row).
   *
   * Default value: "Beaconsmind".
   *
   * Reference: https://developer.android.com/reference/androidx/core/app/NotificationCompat.Builder#setContentTitle(java.lang.CharSequence)
   */
  notificationTitle: string;

  /**
   * Body of the notification (second row).
   *
   * Default value: "Listening for Beacons".
   *
   * Reference: https://developer.android.com/reference/androidx/core/app/NotificationCompat.Builder#setContentText(java.lang.CharSequence)
   */
  notificationText: string;

  /**
   * The user visible name of the channel.
   *
   * Default value: "beaconsmind".
   *
   * Reference: https://developer.android.com/reference/android/app/NotificationChannel#NotificationChannel(java.lang.String,%20java.lang.CharSequence,%20int)
   */
  notificationChannelName: string;
}

/**
 * Initializes the SDK.
 *
 * Call this method as soon as your app starts, so the sdk can track all touchpoints and beacon events correctly.
 *
 * @param options
 * @returns `true` if the SDK was initialized, `false` otherwise.
 */
export function initialize(options: InitializeOptions): Promise<boolean> {
  return Beaconsmind.initialize({
    ...options,
    ...options.platformOptions?.android,
    ...options.platformOptions?.android?.notification,
  });
}

/**
 * **For development purposes only.** Alternative initialization method.
 *
 * Kickstart sdk testing and integration by a single method call. Invokes [initialize] and sets the sdk in development mode.
 *
 * When initialized with this method the sdk requests permissions on it own by.
 *
 * * Your app **must** use activity which implements [ActivityResultCaller].
 *
 * @param options
 * @returns {Response} `true` if the SDK was initialized, `false` otherwise.
 */
export function initializeDevelop(
  options: InitializeOptions
): Promise<boolean> {
  return Beaconsmind.initializeDevelop({
    ...options,
    ...options.platformOptions?.android,
    ...options.platformOptions?.android?.notification,
  });
}

export enum LogLevel {
  /**
   * Any developer-level debug info, repeating events, e.g.:
   * - monitoring/location callbacks,
   * - networking success results.
   */
  debug = 'debug',

  /**
   * Bigger events/steps in the SDK lifecycle, e.g.:
   * - starting/stopping services,
   * - initializing SDK,
   * - one-time events.
   */
  info = 'info',

  /**
   * Allowed, but not-optimal, inconsistent state, e.g.:
   * - trying to start monitoring without permissions.
   */
  warning = 'warning',

  /**
   * Not allowed state (any error).
   */
  error = 'error',

  /**
   * No logging at all.
   */
  silent = 'silent',
}

export interface SignupOptions {
  username: string;
  firstName: string;
  lastName: string;
  password: string;
  confirmPassword: string;
  language?: string;
  gender?: string;
  favoriteStoreID?: number;
  birthDateSeconds?: number;
  countryCode?: string;
}

export interface ImportAccountOptions {
  id: string;
  email: string;
  firstName?: string;
  lastName?: string;
  language?: string;
  gender?: string;
  birthDateSeconds?: number;
}

export interface ProfileResponse {
  id: string;
  userName: string;
  claims: string[];
  roles: string[];
  gender: string | null;
  firstName: string;
  lastName: string;
  fullName: string;
  street: string | null;
  houseNumber: string | null;
  zipCode: string | null;
  city: string | null;
  country: string | null;
  landlinePhone: string | null;
  phoneNumber: string | null;
  url: string | null;
  disablePushNotifications: boolean;
  newsLetterSubscription: boolean;
  joinDate: Date;
  birthDate: Date | null;
  clubId: string | null;
  favoriteStore: string | null;
  favoriteStoreId: number | null;
}

export interface Beacon {
  uuid: string;
  major: string;
  minor: string;
  name: string;
  store: string;
}

/**
 * A class that holds information about the beacon device.
 *
 * It consists of the beacon unique identifier ([uuid], [minor], [major]),
 * the beacon's name ([name]) and the name of the [store] that it is located in.
 *
 * Additionally, it contains data related to its signal strength and the number
 * of times it was contacted.
 */
export interface BeaconContactSummary {
  /**
   * Beacon's uuid.
   */
  uuid: string;

  /**
   * Beacon's major.
   */
  major: string;

  /**
   * Beacon's minor.
   */
  minor: string;

  /**
   * Beacon's name.
   */
  name: string;

  /**
   * The name of the store that the beacon is located in.
   */
  store: string;

  /**
   * Beacon's current signal strength in dB. Null if the beacon has not been contacted yet.
   * RSSI is an abbreviation from Received Signal Strength Indicator.
   * See: https://en.wikipedia.org/wiki/Received_signal_strength_indication
   */
  currentRSSI: number | null;

  /**
   * Beacon's average signal strength in dB (based on the last contacts).
   * Null if the beacon has not been contacted yet.
   */
  averageRSSI: number | null;

  /**
   * Last beacon contact timestamp represented as unix timestamp in milliseconds.
   * Null when the beacon has not been contacted yet.
   */
  lastContactTimestamp: number | null;

  /**
   * Contacts frequency per minute.
   */
  contactFrequencyPerMinute: number;

  /**
   * Indicates how many times the beacon was contacted.
   */
  contactsCount: number;

  /**
   * Returns if the beacon is within range.
   */
  isInRange: boolean;
}

export interface Offer {
  offerId: number;
  title: string;
  text: string;
  imageUrl: string;
  thumbnailUrl: string;
  validity: string;
  isRedeemed: boolean;
  isVoucher: boolean;
  tileAmount: number;
  callToAction: { title: string; link: string } | null;
}

export async function setMinLogLevel(minLogLevel: LogLevel): Promise<any> {
  return Beaconsmind.setMinLogLevel({ minLogLevel });
}

export async function signup(options: SignupOptions): Promise<UserIdResponse> {
  return Beaconsmind.signup(options);
}

export async function importAccount(
  options: ImportAccountOptions
): Promise<UserIdResponse> {
  return Beaconsmind.importAccount(options);
}

export async function login(username: string, password: string): Promise<any> {
  return Beaconsmind.login({ username, password });
}

export async function logout(): Promise<boolean> {
  return Beaconsmind.logout();
}

export async function getOAuthContext(): Promise<UserIdResponse> {
  return Beaconsmind.getOAuthContext();
}

export async function getProfile(): Promise<ProfileResponse> {
  return Beaconsmind.getProfile().then((profile: any) => ({
    ...profile,
    joinDate: new Date(profile.joinDate),
    birthDate: profile.birthDate ? new Date(profile.birthDate) : null,
  }));
}

export async function updateProfile(options: any): Promise<ProfileResponse> {
  return Beaconsmind.updateProfile(options).then((profile: any) => ({
    ...profile,
    joinDate: new Date(profile.joinDate),
    birthDate: profile.birthDate ? new Date(profile.birthDate) : null,
  }));
}

export async function changePassword(options: any): Promise<any> {
  return Beaconsmind.changePassword(options);
}

export async function registerDeviceToken(deviceToken: string): Promise<any> {
  return Beaconsmind.registerDeviceToken({ deviceToken });
}

export async function markOfferAsRead(offerId: number): Promise<any> {
  return Beaconsmind.markOfferAsRead({ offerId });
}

export async function markOfferAsReceived(offerId: number): Promise<any> {
  return Beaconsmind.markOfferAsReceived({ offerId });
}

export async function markOfferAsRedeemed(offerId: number): Promise<any> {
  return Beaconsmind.markOfferAsRedeemed({ offerId });
}

export async function markOfferAsClicked(offerId: number): Promise<any> {
  return Beaconsmind.markOfferAsClicked({ offerId });
}

export async function startListeningBeacons(): Promise<boolean> {
  return Beaconsmind.startListeningBeacons();
}

export async function stopListeningBeacons(): Promise<any> {
  return Beaconsmind.stopListeningBeacons();
}

export async function updateHostname(hostname: string): Promise<any> {
  return Beaconsmind.updateHostname({ hostname });
}

/** @deprecated use getBeaconContactsSummary instead */
export async function getBeaconsSummary(): Promise<Beacon[]> {
  return Beaconsmind.getBeaconsSummary();
}

export async function getBeaconContactsSummary(): Promise<
  BeaconContactSummary[]
> {
  return Beaconsmind.getBeaconContactsSummary();
}

export async function loadOffers(): Promise<Offer[]> {
  return Beaconsmind.loadOffers();
}

export async function loadOffer(offerId: number): Promise<Offer> {
  return Beaconsmind.loadOffer({ offerId });
}
