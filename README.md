# Beaconsmind SDK

[Beaconsmind](https://beaconsmind.com/) SDK for React Native.

## Prerequisites

As a Beaconsmind client, a contained environment is provided for Beaconsmind services being hosted and maintained by
Beaconsmind.

The hostname of the provided environment is required to properly access Beaconsmind API and to use Beaconsmind SDK

> example: `https://adidas.bms.beaconsmind.com`

Please contact Beaconsmind support for the required information about acquiring a valid environment and hostname.

## 1. Installing

using yarn

```sh
  yarn add @beaconsmind/react-native-sdk
```

or npm

```sh
  npm install @beaconsmind/react-native-sdk
```

Make sure your _iOS Deployment Target_ is 11.0 or above.

Make sure your Android _minSdkVersion_ is 23 or above.

### Troubleshooting for iOS

In your `Podfile` specify

```ruby
use_modular_headers!
```

if you get the following error message during `pod install`:

```
The Swift pod `Beaconsmind` depends upon `MBProgressHUD`, which does not define modules.
```

If you get the following compilation error:

```
Import of module 'glog.glog.log_severity' appears within namespace 'google'
```

also add the following to your `Podfile`:

```ruby
pod 'glog', :podspec => '../node_modules/react-native/third-party-podspecs/glog.podspec', :modular_headers => false
```

If there are still compilation errors, try to disable Flipper in your `Podfile`:

```ruby
# use_flipper!()
```

and/or add the following to the `post_install` block

```ruby
post_install do |installer|
    react_native_post_install(installer)
    __apply_Xcode_12_5_M1_post_install_workaround(installer)

    # add this:
    installer.pods_project.targets.each do |target|
      target.build_configurations.each do |config|
        case target.name
        when 'RCT-Folly'
          config.build_settings['IPHONEOS_DEPLOYMENT_TARGET'] = '9.0'
        else
          config.build_settings.delete('IPHONEOS_DEPLOYMENT_TARGET')
        end
      end
    end
end
```

## 2. Permissions

Beaconsmind SDK requires bluetooth and location always permissions to detect nearby beacon devices when running in background and notification permission to send offers.

Consider using the [react-native-permissions](https://www.npmjs.com/package/react-native-permissions) package to request required permissions. Make sure to follow the setup required for native iOS & Android.

See the [demo app](example/) source code for reference.

## 3. Initialize the SDK

This will initialize the SDK and try to retrieve the logged-in user. Call this method as soon as your app starts, so the sdk can track all touchpoints and beacon events correctly.

```ts
import * as Beaconsmind from '@beaconsmind/react-native-sdk';

const initializeResponse = await Beaconsmind.initialize({
  hostname: 'https://test-develop-suite.azurewebsites.net/',
  appVersion: '0.0.1',
  platformOptions: {
    android: {
      usePassiveScanning: true,
      notification: {
        androidNotificationBadgeName: 'ic_beacons',
        androidNotificationChannelName: 'beaconsmind',
        androidNotificationTitle: 'Beaconsmind sdk demo',
        androidNotificationText: 'Listening to beacons',
      },
    },
  },
});
```

## 4. Authentication

### When using Beaconsmind authentication

This approach implies that the customer data is kept in Beaconsmind backend. Use UserManager.login method to perform
customer authentication.

To login existing user.

```ts
const user = await Beaconsmind.login(
  username: usernameValue,
  password: passwordValue,
);
```

To register a new user.

```ts
const user = await Beaconsmind.instance.signup(
    username: usernameValue,
    firstName: firstNameValue,
    lastName: lastnameValue,
    password: passwordValue,
    confirmPassword: confirmPasswordValue,
  );
```

### When using own authentication mechanism

This approach implies that the customer data is kept in clients backend. The authentication is done by the client app
before the Beaconsmind API is used. Whenever customer is authenticated, use `Beaconsmind.importAccount` method
to send basic customer data to Beaconsmind. The customer data is updated only the first time the method is called. The
client app can track if the customer was imported before and omit personal data from following importAccount calls.

```ts
await Beaconsmind.importAccount({
  id: '3287e422-961a-4d47-8eaa-a58e0bd536a1',
  email: 'account@example.com',
  firstName: 'John',
  lastName: 'Doe',
  language: 'en',
  gender: 'male',
  // UNIX timestamp represented in seconds.
  birthDateSeconds: 831589200,
});
```

## 6. Notifications

The SDK uses _APNS_ for iOS, and _Firebase_ for Android. You will need to configure [React Native Firebase](https://rnfirebase.io)
and APNS (e.g. via [React Native Notifications](https://wix.github.io/react-native-notifications/docs/getting-started).

### Setting device token

```ts
await getToken().then((token) => Beaconsmind.registerDeviceToken(token));
```

### Supporting multiple push notification providers

**Issue**

On Android, if there is more than one package used to receive push notifications, for example: [react-native-moengage](https://www.npmjs.com/package/react-native-moengage) & [react-native-notifications](https://www.npmjs.com/package/react-native-notifications), then only one package will be notified about a push notification.

**Explanation**

Each package used to receive push notifications has it’s on `AndroidManifest.xml` file (manifest) where it declares a service that should receive and handle the FCM messages — [here](https://github.com/wix/react-native-notifications/blob/8fb981a99d0504ffb35470d65adb9d6204700381/lib/android/app/src/main/AndroidManifest.xml#L13-L20) is an example from the react-native-notifications package.

When app is built, the app’s manifest is merged with each manifest from packages that it depends on. As a result, the final manifest will contain multiple services declared to receive FCM messages, but the system will deliver push notification only to one of them, the one that is declared earlier.

If there is a service declaration at the app’s manifest, then it will take a precedence over service declarations from packages’ manifest.

**Solution**

In order to deliver push notifications to services declared by each package, a proxy service declared at the app’s manifest needs to be introduced. It will be responsible for intercepting push notifications and forwarding them to other services coming from used packages.

**Implementation**

1. Add [FirebaseMessagingServiceProxy.java](example/android/app/src/main/java/com/beaconsmind/reactdemo/FirebaseMessagingServiceProxy.java) file to your Android app project.
2. In the `FirebaseMessagingServiceProxy` file, update `messagingServices` list with push notification services that you want to notify. Make sure to add necessary imports at the top of the file in order to access them.
3. Register the `FirebaseMessgingServiceProxy` in the [app’s manifest](example/android/app/src/main/AndroidManifest.xml).

## 7. Offers

Beacons ranging is used to pick up offers. In respect to offers, when users receive pushes about a new offer, read an offer and consume an offer, API calls need to be sent to recognize user activity.

### Interacting with offers

- `markOfferAsReceived(offerId)`. Call this when an offer is received via push.
- `markOfferAsRead(offerId)`. Call this when the user opens the offer.
- `markOfferAsRedeemed(offerId)`. Call this when the user redeems the offer.
- `markOfferAsClicked(offerId)`. Call this when the user clicks on the offer's call to action button.

## 8. Beacons

### Interacting with beacons

Beacons feature uses BLE transmitters which can be picked up by devices. In Beaconsmind, BLE transmitters are used to present offers based on a physical location of a device.

Every beacon is defined by it's UUID and major and minor numbers. A list of beacons is persisted and maintained on Beaconsmind environment for each client.

In order to start monitoring nearby beacon devices, call `Beaconsmind.startListeningBeacons()`. Make sure that the bluetooth & location permissions have been granted. To stop the monitoring process, call `Beaconsmind.stopListeningBeacons()`. In order to obtain a list of beacon devices that are within range, call `Beaconsmind.getBeaconContactsSummary()`.

### Read more about beacons

- [iOS setup](https://gitlab.com/beaconsmind/client-sdk-ios/-/blob/main/Documentation/Beacons.md).
- [Android setup](https://gitlab.com/beaconsmind/client-sdk-android/-/blob/main/Documentation/Beacons.md).

## 9. Update user profile

#### BE AWARE: This functionality is supported only when using `Beaconsmind.signup` and NOT `Beaconsmind.importAccount`.

User profile can be updated by calling `Beaconsmind.updateProfile` method. Here is a list of fields that are accepted:

- firstName: string
- lastName: string
- userName: string
- street: string
- city: string
- country: string
- language: string
- birthDateSeconds: timestamp

## 10. Logout

Call `Beaconsmind.logout` to log the user out of the SDK.

## 11. Debugging

Use method `Beaconsmind.setMinLogLevel` to control the amount of logging done by the SDK.

```ts
await Beaconsmind.setMinLogLevel(LogLevel.debug);
await Beaconsmind.setMinLogLevel(LogLevel.info);
await Beaconsmind.setMinLogLevel(LogLevel.warning);
await Beaconsmind.setMinLogLevel(LogLevel.error);
await Beaconsmind.setMinLogLevel(LogLevel.silent);
```
